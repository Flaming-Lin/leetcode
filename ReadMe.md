## 项目背景
算法题刷刷刷！

## 目录结构
- com.flaming
  - [package] leetcode：Leetcode 题目合集
  - [package] model：常用数据结构定义
  - [class] AbstractLeetcodeQuestion：题目抽象类，同时提供了测试用例运行入口
    - 练习方式：PracticeTraversal.class 中修改代码并运行，其他的实现类为答案。

## 重点基础
这些需要反复练习，烂熟于心
- 常用集合类的操作方法：
> Queue<?> queue = new LinkedList<>();
> queue.offer(); queue.add() // 均是向队列中插入一个元素，但是当队列满时，offer 返回 false，而 add 抛出异常 
> queue.poll(); queue.remove() // 均是从队列删除一个元素，但是当队列空时，poll 返回 null，而 remove 抛出异常
> queue.peek(); queue.element() // 均是查询队列的一个元素，类似 poll/delete，队列空时 offer 返回 null，而 element 抛出异常 
>
- 二叉树的遍历模式：BFS(Breadth First Search)、DFS(Depth First Search)-前中后序对应的递归与非递归、Morris中序遍历
- 满二叉树的节点公式：
  - 前n层节点总数 = Math.pow(2, n) - 1
  - 第n层满节点数 = Math.pow(2, n-1)，

## 专项技巧
- BST(Binary Search Tree) 二叉搜索树三大特征：
  - 中序遍历值是从小到大排列的
  - 左节点的最右叶子为 root 的前驱节点
  - 右节点的最左叶子为 root 的后区节点

## 心得 & 踩坑
- 复杂问题优先考虑「递归」进行拆解，并只关注于小任务单一块的逻辑。
- 递归必要的时候可以使用全局变量，整体复杂度会降低很多。
- 边界值可以用包装类与 null 的组合代替纯非包装类。
  - 例如 Leetcode 98 题解法 1 中，专门有 [Integer.MAX_VALUE, Integer.MAX_VALUE] 的 test case，如果有 int 与 Integer.MAX_VALUE 作为边界值判断依据就会被这种 case 针对。