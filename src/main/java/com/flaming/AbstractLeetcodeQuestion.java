package com.flaming;

import java.lang.reflect.Modifier;
import java.util.Scanner;

public abstract class AbstractLeetcodeQuestion {

    /**
     * 测试用例运行
     */
    protected abstract void test();

    /**
     * 测试用例运行前的初始化操作 「尤其类成员变量，在多个 test case 时必须初始化」
     */
    protected void beforeTest() {

    }

    public static void main(String[] args) {
        System.out.print("Please input the [Question] number which you want test: ");
        Scanner scanner = new Scanner(System.in);
        String questionCode = scanner.next();
        if (null == questionCode || questionCode.isEmpty()) {
            System.out.println("What you input is invalid! Please rerun the program!");
            return;
        }
        String className = "com.flaming.leetcode.Question" + questionCode;
        Class<?> clazz = null;
        try {
            clazz = Class.forName(className);
        } catch (ClassNotFoundException ignore) {
        }
        if (null == clazz) {
            System.out.println("The class " + className + " is not found! Please rerun the program!");
            return;
        }

        if (!AbstractLeetcodeQuestion.class.isAssignableFrom(clazz)) {
            System.out.println("The class " + className + " is not a child class of AbstractLeetcodeQuestion! Please rerun the program!");
            return;
        }

        // If the question has multiple answers.
        if (Modifier.isAbstract(clazz.getModifiers())) {
            System.out.print("Please input the [Answer] number which you want test: ");
            String answerCode = scanner.next();
            String answerClassName = className + "_" + answerCode;
            Class<?> answerClazz = null;
            try {
                answerClazz = Class.forName(answerClassName);
            } catch (ClassNotFoundException ignore) {
            }

            if (null == answerClazz) {
                System.out.println("The class " + answerClassName + " is not found! Please rerun the program!");
                return;
            }

            if (!clazz.isAssignableFrom(answerClazz)) {
                System.out.println("The class " + answerClassName + " is not a child class of " + className + "! Please rerun the program!");
                return;
            }
            clazz = answerClazz;
        }

        System.out.println("----------------------- Answer -----------------------");
        try {
            ((AbstractLeetcodeQuestion)clazz.newInstance()).test();
        } catch (Exception e) {
            System.out.println("The class " + className + " run failed! Please rerun the program!");
            System.out.println("Error: " + e.toString());
            for (StackTraceElement element : e.getStackTrace()) {
                System.out.println(element.toString());
            }
        }
    }

}
