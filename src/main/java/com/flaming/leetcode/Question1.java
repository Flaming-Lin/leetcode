package com.flaming.leetcode;

//Given an array of integers nums and an integer target, return indices of the t
//wo numbers such that they add up to target.
//
// You may assume that each input would have exactly one solution, and you may n
//ot use the same element twice.
//
// You can return the answer in any order.
//
//
// Example 1:
//
//
//Input: nums = [2,7,11,15], target = 9
//Output: [0,1]
//Output: Because nums[0] + nums[1] == 9, we return [0, 1].
//
//
// Example 2:
//
//
//Input: nums = [3,2,4], target = 6
//Output: [1,2]
//
//
// Example 3:
//
//
//Input: nums = [3,3], target = 6
//Output: [0,1]
//
//
//
// Constraints:
//
//
// 2 <= nums.length <= 105
// -109 <= nums[i] <= 109
// -109 <= target <= 109
// Only one valid answer exists.
//
// Related Topics 数组 哈希表
// 👍 9494 👎 0

import com.flaming.AbstractLeetcodeQuestion;

import java.util.HashMap;
import java.util.Map;

public class Question1 extends AbstractLeetcodeQuestion {

    public int[] solution1(int[] nums, int target) {
        if (nums.length <= 0) {
            return new int[]{};
        }

        Map<Integer, Integer> valueIndexMap = new HashMap<>(16);
        for (int i = 0; i < nums.length; i++) {
            if (valueIndexMap.containsKey(target - nums[i])) {
                return new int[]{i, valueIndexMap.get(target - nums[i])};
            }
            valueIndexMap.put(nums[i], i);
        }
        return new int[]{};
    }

    @Override
    public void test() {
        for (int num : solution1(new int[]{2, 7, 11, 15}, 9)) {
            System.out.print(num + " ");
        }
    }

}
