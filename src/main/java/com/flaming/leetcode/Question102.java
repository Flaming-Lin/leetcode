package com.flaming.leetcode;

import com.flaming.AbstractLeetcodeQuestion;
import com.flaming.model.TreeNode;

import java.util.List;

//Given a binary tree, return the level order traversal of its nodes' values. (i
//e, from left to right, level by level).
//
//
//For example:
//Given binary tree [3,9,20,null,null,15,7],
//
//    3
//   / \
//  9  20
//    /  \
//   15   7
//
//
//
//return its level order traversal as:
//
//[
//  [3],
//  [9,20],
//  [15,7]
//]
//
// Related Topics 树 广度优先搜索
// 👍 678 👎 0
public abstract class Question102 extends AbstractLeetcodeQuestion {

    protected abstract List<List<Integer>> levelOrder(TreeNode root);

    @Override
    protected void test() {
        TreeNode treeNode = new TreeNode(3,
                new TreeNode(9),
                new TreeNode(20, new TreeNode(15), new TreeNode(7)));
        levelOrder(treeNode).forEach(System.out::println);
    }

}
