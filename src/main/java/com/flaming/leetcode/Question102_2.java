package com.flaming.leetcode;

import com.flaming.model.TreeNode;

import java.util.ArrayList;
import java.util.List;

public class Question102_2 extends Question102{

    /**
     * 第一次比较难想到，说是要做一个 BFS，其实要通过一个 DFS 来实现
     */
    @Override
    protected List<List<Integer>> levelOrder(TreeNode root) {
        if (null == root) {
            return new ArrayList<>();
        }

        List<List<Integer>> resultList = new ArrayList<>();
        fillVal(1, root, resultList);
        return resultList;
    }

    private void fillVal(int level, TreeNode root, List<List<Integer>> resultList) {
        if (null == root) {
            return;
        }
        if (level > resultList.size()) {
            resultList.add(new ArrayList<>());
        }

        resultList.get(level - 1).add(root.val);
        fillVal(level + 1, root.left, resultList);
        fillVal(level + 1, root.right, resultList);
    }
}
