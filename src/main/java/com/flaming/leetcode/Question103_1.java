package com.flaming.leetcode;

import com.flaming.model.TreeNode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class Question103_1 extends Question103{

    @Override
    protected List<List<Integer>> zigzagLevelOrder(TreeNode root) {
        if (null == root) {
            return new ArrayList<>();
        }
        List<List<Integer>> results = new ArrayList<>();
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);
        for (int level = 1; !queue.isEmpty(); level++) {
            List<Integer> curVals = new ArrayList<>();
            int queueSize = queue.size();
            for (int i = 0; i < queueSize; i++) {
                TreeNode treeNode = queue.poll();
                if (level % 2 == 1) {
                    curVals.add(treeNode.val);
                } else {
                    curVals.add(0, treeNode.val);
                }
                if (null != treeNode.left) {
                    queue.offer(treeNode.left);
                }
                if (null != treeNode.right) {
                    queue.offer(treeNode.right);
                }
            }
            results.add(curVals);
        }
        return results;
    }
}
