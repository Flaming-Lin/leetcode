package com.flaming.leetcode;

import com.flaming.model.TreeNode;

import java.util.ArrayList;
import java.util.List;

public class Question103_2 extends Question103{

    @Override
    protected List<List<Integer>> zigzagLevelOrder(TreeNode root) {
        if (null == root) {
            return new ArrayList<>();
        }
        List<List<Integer>> results = new ArrayList<>();
        fillVals(1, root, results);
        return results;
    }

    private void fillVals(int level, TreeNode root, List<List<Integer>> results) {
        if (null == root) {
            return;
        }
        if (level > results.size()) {
            results.add(new ArrayList<>());
        }
        if (level % 2 == 1) {
            results.get(level - 1).add(root.val);
        } else {
            results.get(level - 1).add(0, root.val);
        }
        fillVals(level + 1, root.left, results);
        fillVals(level + 1, root.right, results);
    }
}
