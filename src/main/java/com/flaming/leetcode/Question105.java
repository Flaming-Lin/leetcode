package com.flaming.leetcode;

import com.flaming.AbstractLeetcodeQuestion;
import com.flaming.model.TreeNode;

//Given preorder and inorder traversal of a tree, construct the binary tree.
//
// Note:
//You may assume that duplicates do not exist in the tree.
//
// For example, given
//
//
//preorder = [3,9,20,15,7]
//inorder = [9,3,15,20,7]
//
// Return the following binary tree:
//
//
//    3
//   / \
//  9  20
//    /  \
//   15   7
// Related Topics 树 深度优先搜索 数组
// 👍 737 👎 0
public abstract class Question105 extends AbstractLeetcodeQuestion {

    protected abstract TreeNode buildTree(int[] preorder, int[] inorder);

    @Override
    protected void test() {
        int[] preorder = {3,9,20,15,7};
        int[] inorder = {9,3,15,20,7};
        System.out.println(buildTree(preorder, inorder));
    }
}
