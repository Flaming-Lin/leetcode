package com.flaming.leetcode;

import com.flaming.model.TreeNode;

public class Question105_1 extends Question105{

    @Override
    protected TreeNode buildTree(int[] preorder, int[] inorder) {
        if (preorder.length <= 0 || preorder.length != inorder.length) {
            return null;
        }

        return buildTree(preorder, 0, preorder.length - 1, inorder, 0, inorder.length - 1);
    }

    private TreeNode buildTree(int[] preorder, int preLeft, int preRight,
                               int[] inorder, int inLeft, int inRight) {
        if (inLeft > inRight || preLeft > preRight) {
            return null;
        }
        // Get a root in preoder.
        int rootVal = preorder[preLeft];
        // Find the root in inorder.
        int inIndex = -1;
        for (int i = inLeft; i <= inRight; i++) {
            if (inorder[i] == rootVal) {
                inIndex = i;
                break;
            }
        }
        // Build child tree.
        return new TreeNode(rootVal,
                buildTree(preorder, preLeft + 1, preLeft + inIndex - inLeft , inorder, inLeft, inIndex - 1),
                buildTree(preorder, preLeft + 1 + inIndex - inLeft, preRight, inorder, inIndex + 1, inRight));
    }
}
