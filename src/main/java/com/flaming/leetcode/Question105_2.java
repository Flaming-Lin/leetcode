package com.flaming.leetcode;

import com.flaming.model.TreeNode;

import java.util.Stack;

public class Question105_2 extends Question105{

    /**
     * 本方法基于如下发现：对于前序遍历中的相邻节点 x,y，仅有两种可能
     * - y 是 x 的左子树
     * - y 是 x或x父节点或x父节点's父节点 的右子树
     */
    @Override
    protected TreeNode buildTree(int[] preorder, int[] inorder) {
        if (preorder.length <= 0 || preorder.length != inorder.length) {
            return null;
        }
        Stack<TreeNode> stack = new Stack<>();
        TreeNode root = new TreeNode(preorder[0]);
        stack.push(root);
        for (int preIndex = 1, inIndex = 0; preIndex < preorder.length; preIndex++) {
            TreeNode curNode = stack.peek();
            if (curNode.val != inorder[inIndex]) {
                curNode.left = new TreeNode(preorder[preIndex]);
                stack.push(curNode.left);
            } else {
                while (!stack.isEmpty() && stack.peek().val == inorder[inIndex]) {
                    curNode = stack.pop();
                    inIndex++;
                }
                curNode.right = new TreeNode(preorder[preIndex]);
                stack.push(curNode.right);
            }
        }
        return root;
    }
}
