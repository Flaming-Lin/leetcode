package com.flaming.leetcode;

import com.flaming.AbstractLeetcodeQuestion;
import com.flaming.model.TreeNode;

//Given inorder and postorder traversal of a tree, construct the binary tree.
//
// Note:
//You may assume that duplicates do not exist in the tree.
//
// For example, given
//
//
//inorder = [9,3,15,20,7]
//postorder = [9,15,7,20,3]
//
// Return the following binary tree:
//
//
//    3
//   / \
//  9  20
//    /  \
//   15   7
//
// Related Topics 树 深度优先搜索 数组
// 👍 396 👎 0
public abstract class Question106 extends AbstractLeetcodeQuestion {

    protected abstract TreeNode buildTree(int[] inorder, int[] postorder);

    @Override
    protected void test() {
        int[] inorder = {9,3,15,20,7};
        int[] postorder = {9,15,7,20,3};
        System.out.println(buildTree(inorder, postorder));
    }
}
