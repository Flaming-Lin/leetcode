package com.flaming.leetcode;

import com.flaming.model.TreeNode;

public class Question106_1 extends Question106{

    @Override
    protected TreeNode buildTree(int[] inorder, int[] postorder) {
        if (inorder.length <= 0 || inorder.length != postorder.length) {
            return null;
        }
        return buildTree(inorder, 0, inorder.length - 1, postorder, 0, postorder.length - 1);
    }

    private TreeNode buildTree(int[] inorder, int inLeft, int inRight,
                               int[] postorder, int postLeft, int postRight) {
        if (inLeft > inRight || postLeft > postRight) {
            return null;
        }
        TreeNode root = new TreeNode(postorder[postRight]);
        int rootIndex = -1;
        for (int i = inLeft; i <= inRight; i++) {
            if (inorder[i] == root.val) {
                rootIndex = i;
                break;
            }
        }
        root.left = buildTree(inorder, inLeft, rootIndex - 1, postorder, postLeft, postLeft + rootIndex - inLeft - 1);
        root.right = buildTree(inorder, rootIndex + 1, inRight, postorder, postLeft + rootIndex - inLeft, postRight - 1);
        return root;
    }
}
