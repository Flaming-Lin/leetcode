package com.flaming.leetcode;

import com.flaming.model.TreeNode;

import java.util.Stack;

public class Question106_2 extends Question106{

    /**
     * 本方法基于如下发现：对于前序遍历中的相邻节点 x,y，仅有两种可能
     * - x 是 y 的右子树
     * - x 是 y或y父节点或y父节点's父节点 的左子树
     *
     * 可以理解为 105 题的反向，这里面比较重要的一个点是：
     * - 倒置的后序遍历结果 = 左右互换的前序遍历
     */
    @Override
    protected TreeNode buildTree(int[] inorder, int[] postorder) {
        if (inorder.length <= 0 || inorder.length != postorder.length) {
            return null;
        }

        Stack<TreeNode> stack = new Stack<>();
        TreeNode root = new TreeNode(postorder[postorder.length - 1]);
        stack.push(root);
        int inIndex = inorder.length - 1;
        for (int postIndex = postorder.length - 2; postIndex >= 0; postIndex--) {
            TreeNode curNode = stack.peek();
            if (curNode.val != inorder[inIndex]) {
                curNode.right = new TreeNode(postorder[postIndex]);
                stack.push(curNode.right);
            } else {
                while (!stack.isEmpty() && stack.peek().val == inorder[inIndex]) {
                    curNode = stack.pop();
                    inIndex--;
                }
                curNode.left = new TreeNode(postorder[postIndex]);
                stack.push(curNode.left);
            }
        }
        return root;
    }
}
