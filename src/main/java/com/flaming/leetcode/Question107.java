package com.flaming.leetcode;

import com.flaming.AbstractLeetcodeQuestion;
import com.flaming.model.TreeNode;

import java.util.List;

//Given a binary tree, return the bottom-up level order traversal of its nodes'
//values. (ie, from left to right, level by level from leaf to root).
//
//
//For example:
//Given binary tree [3,9,20,null,null,15,7],
//
//    3
//   / \
//  9  20
//    /  \
//   15   7
//
//
//
//return its bottom-up level order traversal as:
//
//[
//  [15,7],
//  [9,20],
//  [3]
//]
//
// Related Topics 树 广度优先搜索
// 👍 358 👎 0
public abstract class Question107 extends AbstractLeetcodeQuestion {

    protected abstract List<List<Integer>> levelOrderBottom(TreeNode root);

    @Override
    protected void test() {
        TreeNode treeNode = new TreeNode(3,
                new TreeNode(9),
                new TreeNode(20, new TreeNode(15), new TreeNode(7)));
        System.out.println(levelOrderBottom(treeNode));
    }
}
