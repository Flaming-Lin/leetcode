package com.flaming.leetcode;

import com.flaming.model.TreeNode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Question107_2 extends Question107{

    @Override
    protected List<List<Integer>> levelOrderBottom(TreeNode root) {
        if (null == root) {
            return new ArrayList<>();
        }
        List<List<Integer>> results = new ArrayList<>();
        fillVal(1, root, results);
        Collections.reverse(results);
        return results;
    }

    private void fillVal(int level, TreeNode root, List<List<Integer>> results) {
        if (null == root) {
            return;
        }
        if (level > results.size()) {
            results.add(new ArrayList<>());
        }
        results.get(level - 1).add(root.val);
        fillVal(level + 1, root.left, results);
        fillVal(level + 1, root.right, results);
    }
}
