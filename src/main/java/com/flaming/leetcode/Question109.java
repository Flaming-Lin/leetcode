package com.flaming.leetcode;

import com.flaming.AbstractLeetcodeQuestion;
import com.flaming.model.ListNode;
import com.flaming.model.TreeNode;

import java.util.ArrayList;
import java.util.List;

//Given the head of a singly linked list where elements are sorted in ascending
//order, convert it to a height balanced BST.
//
// For this problem, a height-balanced binary tree is defined as a binary tree
//in which the depth of the two subtrees of every node never differ by more than 1.
//
//
//
// Example 1:
//
//
//Input: head = [-10,-3,0,5,9]
//Output: [0,-3,9,-10,null,5]
//Explanation: One possible answer is [0,-3,9,-10,null,5], which represents the
//shown height balanced BST.
//
//
// Example 2:
//
//
//Input: head = []
//Output: []
//
//
//
// Constraints:
//
//
// The number of nodes in head is in the range [0, 2 * 10⁴].
// -10⁵ <= Node.val <= 10⁵
//
// Related Topics 树 二叉搜索树 链表 分治 二叉树 👍 648 👎 0
public abstract class Question109 extends AbstractLeetcodeQuestion {

    public abstract TreeNode sortedListToBST(ListNode head);

    @Override
    protected void test() {
        ListNode head = new ListNode(new int[]{-10, -3, 0, 5, 9});
        System.out.println(sortedListToBST(head));
        head = new ListNode(new int[]{0, 1, 2, 3, 4, 5});
        System.out.println(sortedListToBST(head));
    }

}
