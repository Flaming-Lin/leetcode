package com.flaming.leetcode;

import com.flaming.model.ListNode;
import com.flaming.model.TreeNode;

import java.util.ArrayList;
import java.util.List;

public class Question109_1 extends Question109 {

    /**
     * 最无脑的做法，先把 list 转成 array...
     */
    @Override
    public TreeNode sortedListToBST(ListNode head) {
        if (null == head) {
            return null;
        }
        List<Integer> list = new ArrayList<>();
        while (null != head) {
            list.add(head.val);
            head = head.next;
        }
        Integer[] arr = list.toArray(new Integer[0]);

        return buildTree(arr, 0, arr.length - 1);
    }

    private TreeNode buildTree(Integer[] arr, int start, int end) {
        if (start > end) {
            return null;
        }
        int mid = (start + end + 1) / 2;
        TreeNode root = new TreeNode(arr[mid]);
        root.left = buildTree(arr, start, mid - 1);
        root.right = buildTree(arr, mid + 1, end);
        return root;
    }

}
