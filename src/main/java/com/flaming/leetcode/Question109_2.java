package com.flaming.leetcode;

import com.flaming.model.ListNode;
import com.flaming.model.TreeNode;

public class Question109_2 extends Question109 {

    @Override
    public TreeNode sortedListToBST(ListNode head) {
        if (null == head) {
            return null;
        }

        return buildTree(head, null);
    }

    private TreeNode buildTree(ListNode start, ListNode end) {
        if (start == end) {
            return null;
        }

        ListNode mid = getMidNode(start, end);
        TreeNode root = new TreeNode(mid.val);
        root.left = buildTree(start, mid);
        root.right = buildTree(mid.next, end);
        return root;
    }

    private ListNode getMidNode(ListNode left, ListNode right) {
        if (null == left) {
            return null;
        }
        ListNode fast = left;
        ListNode slow = left;
        while (right != fast && right != fast.next) {
            fast = fast.next;
            fast = fast.next;
            slow = slow.next;
        }
        return slow;
    }

}
