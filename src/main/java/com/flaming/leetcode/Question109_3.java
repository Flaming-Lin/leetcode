package com.flaming.leetcode;

import com.flaming.model.ListNode;
import com.flaming.model.TreeNode;

public class Question109_3 extends Question109 {

    private ListNode globalHead;

    @Override
    public TreeNode sortedListToBST(ListNode head) {
        if (null == head) {
            return null;
        }
        globalHead = head;
        int length = getLength(head);
        return buildTree(0, length - 1);
    }

    private TreeNode buildTree(int left, int right) {
        if (left > right) {
            return null;
        }

        int mid = (left + right + 1) / 2;
        TreeNode root = new TreeNode(-1);
        root.left = buildTree(left, mid - 1);
        root.val = globalHead.val;
        globalHead = globalHead.next;
        root.right = buildTree(mid + 1, right);
        return root;
    }

    private int getLength(ListNode head) {
        int length = 0;
        while (null != head) {
            length++;
            head = head.next;
        }
        return length;
    }

}
