package com.flaming.leetcode;

import com.flaming.model.TreeNode;

public class Question110_1 extends Question110 {

    /**
     * Solution 1
     * getMaxDepth 会被重复调用，容易想到，但性能较差。
     */
    @Override
    public boolean isBalanced(TreeNode root) {
        if (null == root) {
            return true;
        }

        return isBalanced(root.left) && isBalanced(root.right) && Math.abs(getMaxDepth(root.left) - getMaxDepth(root.right)) <= 1;
    }

    private int getMaxDepth(TreeNode root) {
        if (null == root) {
            return 0;
        }
        return Math.max(getMaxDepth(root.left), getMaxDepth(root.right)) + 1;
    }
}
