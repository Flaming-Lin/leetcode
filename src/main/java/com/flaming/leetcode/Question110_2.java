package com.flaming.leetcode;

import com.flaming.model.TreeNode;

public class Question110_2 extends Question110{

    /**
     * Solution 2
     * 最佳解法，从底向上遍历
     */
    @Override
    public boolean isBalanced(TreeNode root) {
        return getMaxDepth(root) != -1;
    }

    public int getMaxDepth(TreeNode root) {
        if (null == root) {
            return 0;
        }
        int leftDepth = getMaxDepth(root.left);
        if (-1 == leftDepth) {
            return -1;
        }
        int rightDepth = getMaxDepth(root.right);
        if (-1 == rightDepth) {
            return -1;
        }
        return Math.abs(rightDepth - leftDepth) <= 1 ? Math.max(leftDepth, rightDepth) + 1 : -1;
    }
}
