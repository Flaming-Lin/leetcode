package com.flaming.leetcode;

import com.flaming.model.TreeNode;

import java.util.ArrayList;
import java.util.List;

public class Question113_1 extends Question113{

    /**
     * Solution 1
     *
     * 注意使用 new ArrayList 进行每次的深拷贝，不过这种深拷贝性能较差
     */
    @Override
    public List<List<Integer>> pathSum(TreeNode root, int sum) {
        List<List<Integer>> resultList = new ArrayList<>();
        pathSum(root, sum, new ArrayList<>(), resultList);
        return resultList;
    }

    private void pathSum(TreeNode root, int sum, List<Integer> curVals, List<List<Integer>> resultList) {
        if (null == root) {
            return;
        }
        curVals.add(root.val);
        if (null == root.left && null == root.right && root.val == sum) {
            resultList.add(curVals);
            return;
        }
        pathSum(root.left, sum - root.val, new ArrayList<>(curVals), resultList);
        pathSum(root.right, sum - root.val, new ArrayList<>(curVals), resultList);
    }
}
