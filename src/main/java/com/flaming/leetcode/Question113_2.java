package com.flaming.leetcode;

import com.flaming.model.TreeNode;

import java.util.ArrayList;
import java.util.List;

public class Question113_2 extends Question113{

    /**
     * Solution 2
     */
    @Override
    public List<List<Integer>> pathSum(TreeNode root, int sum) {
        List<List<Integer>> resultList = new ArrayList<>();
        pathSum(root, sum, new ArrayList<>(), resultList);
        return resultList;
    }

    private void pathSum(TreeNode root, int sum, List<Integer> curVals, List<List<Integer>> resultList) {
        if (null == root) {
            return;
        }
        curVals.add(root.val);
        if (null == root.left && null == root.right && root.val == sum) {
            resultList.add(new ArrayList<>(curVals));
            curVals.remove(curVals.size() - 1);
            return;
        }
        pathSum(root.left, sum - root.val, curVals, resultList);
        pathSum(root.right, sum - root.val, curVals, resultList);
        curVals.remove(curVals.size() - 1);
    }


}
