package com.flaming.leetcode;

import com.flaming.AbstractLeetcodeQuestion;
import com.flaming.model.TreeNode;

//Given a binary tree, flatten it to a linked list in-place. 
//
// For example, given the following tree: 
//
// 
//    1
//   / \
//  2   5
// / \   \
//3   4   6
// 
//
// The flattened tree should look like: 
//
// 
//1
// \
//  2
//   \
//    3
//     \
//      4
//       \
//        5
//         \
//          6
// 
// Related Topics 树 深度优先搜索 
// 👍 606 👎 0
public class Question114 extends AbstractLeetcodeQuestion {

    public void flatten(TreeNode root) {
        if (null == root) {
            return;
        }

        flatten(root.left);
        flatten(root.right);

        TreeNode right = root.right;
        root.right = root.left;
        root.left = null;
        if (null == right) {
            return;
        }
        TreeNode curNode = root;
        while (null != curNode.right) {
            curNode = curNode.right;
        }
        curNode.right = right;
    }

    @Override
    protected void test() {
        TreeNode treeNode = new TreeNode(1,
                new TreeNode(2, new TreeNode(3), new TreeNode(4)),
                new TreeNode(5, null, new TreeNode(6)));
        System.out.println("Before: " + treeNode.toString());
        flatten(treeNode);
        System.out.println("After: " + treeNode.toString());
    }

}
