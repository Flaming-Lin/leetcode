package com.flaming.leetcode;

import com.flaming.AbstractLeetcodeQuestion;

import java.util.LinkedList;
import java.util.Queue;

//You are given a perfect binary tree where all leaves are on the same level, an
//d every parent has two children. The binary tree has the following definition:
//
//
//struct Node {
//  int val;
//  Node *left;
//  Node *right;
//  Node *next;
//}
//
//
// Populate each next pointer to point to its next right node. If there is no ne
//xt right node, the next pointer should be set to NULL.
//
// Initially, all next pointers are set to NULL.
//
//
//
// Follow up:
//
//
// You may only use constant extra space.
// Recursive approach is fine, you may assume implicit stack space does not coun
//t as extra space for this problem.
//
//
//
// Example 1:
//
//
//
//
//Input: root = [1,2,3,4,5,6,7]
//Output: [1,#,2,3,#,4,5,6,7,#]
//Explanation: Given the above perfect binary tree (Figure A), your function sho
//uld populate each next pointer to point to its next right node, just like in Fig
//ure B. The serialized output is in level order as connected by the next pointers
//, with '#' signifying the end of each level.
//
//
//
// Constraints:
//
//
// The number of nodes in the given tree is less than 4096.
// -1000 <= node.val <= 1000
// Related Topics 树 深度优先搜索 广度优先搜索
// 👍 334 👎 0
public abstract class Question116 extends AbstractLeetcodeQuestion {

    public abstract Node connect(Node root);

    @Override
    protected void test() {
        Node root = new Node(1,
                new Node(2, new Node(4), new Node(5), null),
                new Node(3, new Node(6), new Node(7), null),
                null);
        System.out.println(connect(root));
    }

    class Node {
        public int val;
        public Node left;
        public Node right;
        public Node next;

        public Node() {}

        public Node(int _val) {
            val = _val;
        }

        public Node(int _val, Node _left, Node _right, Node _next) {
            val = _val;
            left = _left;
            right = _right;
            next = _next;
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            Queue<Node> queue = new LinkedList<>();
            Node curNode = this;
            while (null != curNode) {
                queue.offer(curNode);
                curNode = curNode.left;
            }
            while (!queue.isEmpty()) {
                curNode = queue.poll();
                while (null != curNode) {
                    sb.append(curNode.val);
                    sb.append(",");
                    curNode = curNode.next;
                }
                sb.append("#,");
            }
            return sb.toString();
        }
    }
}
