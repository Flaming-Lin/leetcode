package com.flaming.leetcode;

public class Question116_1 extends Question116 {

    public Node connect(Node root) {
        if (null == root) {
            return null;
        }
        connect(root.left, root.right);
        return root;
    }

    private void connect(Node leftNode, Node rightNode) {
        if (null == leftNode || null == rightNode) {
            return;
        }
        leftNode.next = rightNode;
        connect(leftNode.left, leftNode.right);
        connect(rightNode.left, rightNode.right);
        connect(leftNode.right, rightNode.left);
    }

}
