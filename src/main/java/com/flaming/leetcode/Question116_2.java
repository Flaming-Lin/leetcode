package com.flaming.leetcode;

import java.util.LinkedList;
import java.util.Queue;

public class Question116_2 extends Question116{

    /**
     * 此种方法用了 O(n) 的空间，不满足题目要求
     */
    @Override
    public Node connect(Node root) {
        if (null == root) {
            return null;
        }
        Queue<Node> queue = new LinkedList<>();
        queue.offer(root);
        while (!queue.isEmpty()) {
            int queueSize = queue.size();
            Node preNode = null;
            for (int i = 0; i < queueSize; i++) {
                Node curNode = queue.poll();
                if (null != preNode) {
                    preNode.next = curNode;
                }
                preNode = curNode;
                if (null != curNode.left) {
                    queue.offer(curNode.left);
                }
                if (null != curNode.right) {
                    queue.offer(curNode.right);
                }
            }
        }
        return root;
    }
}
