package com.flaming.leetcode;

public class Question116_3 extends Question116{

    @Override
    public Node connect(Node root) {
        if (null == root) {
            return null;
        }
        Node curLeftNode = root;
        while (null != curLeftNode.left) {
            Node curNode = curLeftNode;
            while (null != curNode) {
                curNode.left.next = curNode.right;
                if (null != curNode.next) {
                    curNode.right.next = curNode.next.left;
                }
                curNode = curNode.next;
            }
            curLeftNode = curLeftNode.left;
        }
        return root;
    }
}
