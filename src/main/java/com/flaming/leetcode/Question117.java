package com.flaming.leetcode;

import com.flaming.AbstractLeetcodeQuestion;

import java.util.LinkedList;
import java.util.Queue;

//Given a binary tree
//
//
//struct Node {
//  int val;
//  Node *left;
//  Node *right;
//  Node *next;
//}
//
//
// Populate each next pointer to point to its next right node. If there is no ne
//xt right node, the next pointer should be set to NULL.
//
// Initially, all next pointers are set to NULL.
//
//
//
// Follow up:
//
//
// You may only use constant extra space.
// Recursive approach is fine, you may assume implicit stack space does not coun
//t as extra space for this problem.
//
//
//
// Example 1:
//
//
//
//
//Input: root = [1,2,3,4,5,null,7]
//Output: [1,#,2,3,#,4,5,7,#]
//Explanation: Given the above binary tree (Figure A), your function should popu
//late each next pointer to point to its next right node, just like in Figure B. T
//he serialized output is in level order as connected by the next pointers, with '
//#' signifying the end of each level.
//
//
//
// Constraints:
//
//
// The number of nodes in the given tree is less than 6000.
// -100 <= node.val <= 100
//
// Related Topics 树 深度优先搜索
// 👍 313 👎 0
public abstract class Question117 extends AbstractLeetcodeQuestion {

    protected abstract Node connect(Node root);

    @Override
    protected void test() {
        Node root = new Node(1,
                new Node(2, new Node(4), new Node(5), null),
                new Node(3, null, new Node(7), null),
                null);
        System.out.println(connect(root));
        root = new Node(3,
                new Node(9),
                new Node(20, new Node(15), new Node(7), null),
                null);
        System.out.println(connect(root));
        root = new Node(1, null, new Node(2), null);
        System.out.println(connect(root));
    }

    class Node {
        public int val;
        public Node left;
        public Node right;
        public Node next;

        public Node() {}

        public Node(int _val) {
            val = _val;
        }

        public Node(int _val, Node _left, Node _right, Node _next) {
            val = _val;
            left = _left;
            right = _right;
            next = _next;
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            Node nextLevelNode = this;
            while (null != nextLevelNode) {
                Node curLevelNode = nextLevelNode;
                nextLevelNode = null;
                while (null != curLevelNode) {
                    sb.append(curLevelNode.val);
                    sb.append(",");
                    if (null == nextLevelNode && null != curLevelNode.left) {
                        nextLevelNode = curLevelNode.left;
                    }
                    if (null == nextLevelNode && null != curLevelNode.right) {
                        nextLevelNode = curLevelNode.right;
                    }
                    curLevelNode = curLevelNode.next;
                }
                sb.append("#,");
            }
            return sb.toString();
        }
    }
}
