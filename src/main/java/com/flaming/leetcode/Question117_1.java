package com.flaming.leetcode;

public class Question117_1 extends Question117{

    @Override
    protected Node connect(Node root) {
        if (null == root) {
            return null;
        }
        Node nextLevelLeftNode = root;
        while (null != nextLevelLeftNode) {
            Node curLevelNode = nextLevelLeftNode;
            Node nextLevelPreNode = null;
            nextLevelLeftNode = null;
            while (null != curLevelNode) {
                if (null != curLevelNode.left) {
                    if (null != nextLevelPreNode) {
                        nextLevelPreNode.next = curLevelNode.left;
                        nextLevelPreNode = nextLevelPreNode.next;
                    }
                    if (null == nextLevelLeftNode) {
                        nextLevelLeftNode = nextLevelPreNode = curLevelNode.left;
                    }
                }
                if (null != curLevelNode.right) {
                    if (null != nextLevelPreNode) {
                        nextLevelPreNode.next = curLevelNode.right;
                        nextLevelPreNode = nextLevelPreNode.next;
                    }
                    if (null == nextLevelLeftNode) {
                        nextLevelLeftNode = nextLevelPreNode = curLevelNode.right;
                    }
                }
                curLevelNode = curLevelNode.next;
            }
        }
        return root;
    }

}
