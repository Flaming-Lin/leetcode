package com.flaming.leetcode;

public class Question11_1 extends Question11{

    /**
     * 暴力破解，可以忽略了
     */
    @Override
    protected int maxArea(int[] height) {
        int maxArea = 0;

        for (int i = 0; i < height.length; i++) {
            for (int j = i + 1; j < height.length; j++) {
                int area = Math.min(height[i], height[j]) * (j - i);
                if (area > maxArea) {
                    maxArea = area;
                }
            }
        }
        return maxArea;
    }
}
