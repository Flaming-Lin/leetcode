package com.flaming.leetcode;

public class Question11_2 extends Question11 {

    /**
     * 双指针最佳解法。核心在于「从两侧建立双指针，每次需要向内移动数字较小的指针」
     *
     * - 假设双指针的值 X 和 Y(X < Y)，相距 T。则 area = Math.min(X, Y) * T。
     *   但如果移动了 Y，在 T 减小的同时，X 卡死了上限，永远不会有更大的值。所以要移动较小数字的指针。
     */
    @Override
    protected int maxArea(int[] height) {
        int maxArea = 0;

        for (int left = 0, right = height.length - 1; left < right;) {
            int area = Math.min(height[left], height[right]) * (right - left);
            if (area > maxArea) {
                maxArea = area;
            }
            if (height[left] < height[right]) {
                left++;
            } else {
                right--;
            }
        }
        return maxArea;
    }
}
