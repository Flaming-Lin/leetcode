package com.flaming.leetcode;

import com.flaming.AbstractLeetcodeQuestion;
import com.flaming.model.TreeNode;

//A path in a binary tree is a sequence of nodes where each pair of adjacent nod
//es in the sequence has an edge connecting them. A node can only appear in the se
//quence at most once. Note that the path does not need to pass through the root.
//
//
// The path sum of a path is the sum of the node's values in the path.
//
// Given the root of a binary tree, return the maximum path sum of any path.
//
//
// Example 1:
//
//
//Input: root = [1,2,3]
//Output: 6
//Explanation: The optimal path is 2 -> 1 -> 3 with a path sum of 2 + 1 + 3 = 6.
//
//
//
// Example 2:
//
//
//Input: root = [-10,9,20,null,null,15,7]
//Output: 42
//Explanation: The optimal path is 15 -> 20 -> 7 with a path sum of 15 + 20 + 7
//= 42.
//
//
//
// Constraints:
//
//
// The number of nodes in the tree is in the range [1, 3 * 104].
// -1000 <= Node.val <= 1000
//
// Related Topics 树 深度优先搜索 递归
// 👍 898 👎 0
public class Question124 extends AbstractLeetcodeQuestion {

    private int maxResult = Integer.MIN_VALUE;

    /**
     * 核心仍然是拆到每个节点单独来看，简化逻辑。
     *
     * - 节点能贡献的最大值是 max(leftCommit + root, rightCommit + root)
     * - 如果以该节点作为跟节点，则可以最大有 leftCommit + rightCommit + root
     */
    public int maxPathSum(TreeNode root) {
        traversal(root);
        return maxResult;
    }

    public int traversal(TreeNode root) {
        if (null == root) {
            return 0;
        }

        int leftMaxSum = Math.max(traversal(root.left), 0);
        int rightMaxSum = Math.max(traversal(root.right), 0);

        maxResult = Math.max(maxResult, leftMaxSum + rightMaxSum + root.val);

        return Math.max(leftMaxSum, rightMaxSum) + root.val;
    }

    @Override
    protected void test() {
        TreeNode root = new TreeNode(-10, new TreeNode(9), new TreeNode(20, new TreeNode(15), new TreeNode(7)));
        System.out.println(maxPathSum(root));
        this.beforeTest();
        root = new TreeNode(-3);
        System.out.println(maxPathSum(root));
        this.beforeTest();
        root = new TreeNode(1,
                new TreeNode(-2, new TreeNode(1, new TreeNode(-1), null), new TreeNode(3)),
                new TreeNode(-3, new TreeNode(-2), null));
        System.out.println(maxPathSum(root));
        this.beforeTest();
        root = new TreeNode(5,
                new TreeNode(4, new TreeNode(11, new TreeNode(7), new TreeNode(2)), null),
                new TreeNode(8, new TreeNode(13), new TreeNode(4, new TreeNode(1), null)));
        System.out.println(maxPathSum(root));
    }

    @Override
    protected void beforeTest() {
        this.maxResult = Integer.MIN_VALUE;
    }
}
