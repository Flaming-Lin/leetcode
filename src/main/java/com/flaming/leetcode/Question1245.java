package com.flaming.leetcode;

import com.flaming.AbstractLeetcodeQuestion;

/*
给定一棵树，求出这棵树的直径，即树上最远两点的距离。
示例1的树如下图所示。其中4到5之间的路径最长，是树的直径，距离为5+2+4=11

            0
          3/
          1
        2/ \4
        2   5
      1/ \5
      3   4

示例1
输入
复制
6,[[0,1],[1,5],[1,2],[2,3],[2,4]],[3,4,2,1,5]
返回值
复制
11
 */
public class Question1245 extends AbstractLeetcodeQuestion {

    static class Interval {
        int start;
        int end;
    }

    /**
     * 树的直径
     * @param n int整型 树的节点个数
     * @param Tree_edge Interval类一维数组 树的边
     * @param Edge_value int整型一维数组 边的权值
     * @return int整型
     */
    public int solve (int n, Interval[] Tree_edge, int[] Edge_value) {
        // TODO: write code here
        return 0;
    }

    @Override
    protected void test() {

    }
}
