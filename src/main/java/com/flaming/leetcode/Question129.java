package com.flaming.leetcode;

import com.flaming.AbstractLeetcodeQuestion;
import com.flaming.model.TreeNode;

//Given a binary tree containing digits from 0-9 only, each root-to-leaf path co
//uld represent a number.
//
// An example is the root-to-leaf path 1->2->3 which represents the number 123.
//
//
// Find the total sum of all root-to-leaf numbers.
//
// Note: A leaf is a node with no children.
//
// Example:
//
//
//Input: [1,2,3]
//    1
//   / \
//  2   3
//Output: 25
//Explanation:
//The root-to-leaf path 1->2 represents the number 12.
//The root-to-leaf path 1->3 represents the number 13.
//Therefore, sum = 12 + 13 = 25.
//
// Example 2:
//
//
//Input: [4,9,0,5,1]
//    4
//   / \
//  9   0
// / \
//5   1
//Output: 1026
//Explanation:
//The root-to-leaf path 4->9->5 represents the number 495.
//The root-to-leaf path 4->9->1 represents the number 491.
//The root-to-leaf path 4->0 represents the number 40.
//Therefore, sum = 495 + 491 + 40 = 1026.
// Related Topics 树 深度优先搜索
// 👍 281 👎 0
public abstract class Question129 extends AbstractLeetcodeQuestion {

    protected abstract int sumNumbers(TreeNode root);

    @Override
    protected void test() {
        TreeNode treeNode = new TreeNode(4,
                new TreeNode(9, new TreeNode(5), new TreeNode(1)),
                new TreeNode(0));
        System.out.println(sumNumbers(treeNode));
    }
}
