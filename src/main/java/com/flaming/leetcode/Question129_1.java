package com.flaming.leetcode;

import com.flaming.model.TreeNode;

public class Question129_1 extends Question129{

    @Override
    protected int sumNumbers(TreeNode root) {
        return sumNumbers(root, 0);
    }

    private int sumNumbers(TreeNode root, int baseSum) {
        if (null == root) {
            return 0;
        }
        baseSum = baseSum * 10 + root.val;;
        if (null == root.left && null == root.right) {
            return baseSum;
        }
        return sumNumbers(root.left, baseSum) + sumNumbers(root.right, baseSum);
    }
}
