package com.flaming.leetcode;

import com.flaming.model.TreeNode;

import java.util.Stack;

public class Question129_2 extends Question129{

    @Override
    protected int sumNumbers(TreeNode root) {
        if (null == root) {
            return 0;
        }
        Stack<TreeNode> nodeStack = new Stack<>();
        Stack<Integer> sumStack = new Stack<>();
        nodeStack.push(root);
        sumStack.push(root.val);
        int result = 0;
        while (!nodeStack.isEmpty()) {
            TreeNode curNode = nodeStack.pop();
            Integer curSum = sumStack.pop();
            if (null == curNode.left && null == curNode.right) {
                result += curSum;
                continue;
            }
            if (null != curNode.left) {
                nodeStack.push(curNode.left);
                sumStack.push(curSum * 10 + curNode.left.val);
            }
            if (null != curNode.right) {
                nodeStack.push(curNode.right);
                sumStack.push(curSum * 10 + curNode.right.val);
            }
        }
        return result;
    }
}
