package com.flaming.leetcode;

import com.flaming.model.TreeNode;

import java.util.ArrayList;
import java.util.List;

public class Question144_1 extends Question144{

    @Override
    protected List<Integer> preorderTraversal(TreeNode root) {
        List<Integer> results = new ArrayList<>();
        preorderTraversal(root, results);
        return results;
    }

    private void preorderTraversal(TreeNode root, List<Integer> results) {
        if (null == root) {
            return;
        }
        results.add(root.val);
        preorderTraversal(root.left, results);
        preorderTraversal(root.right, results);
    }
}
