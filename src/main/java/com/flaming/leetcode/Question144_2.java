package com.flaming.leetcode;

import com.flaming.model.TreeNode;

import java.util.*;

public class Question144_2 extends Question144{

    @Override
    protected List<Integer> preorderTraversal(TreeNode root) {
        if (null == root) {
            return new ArrayList<>();
        }
        List<Integer> results = new ArrayList<>();
        Stack<TreeNode> stack = new Stack<>();
        TreeNode curNode = root;
        while (!stack.isEmpty() || null != curNode) {
            if (null == curNode) {
                curNode = stack.pop();
                continue;
            }
            while (null != curNode) {
                results.add(curNode.val);
                stack.push(curNode.right);
                curNode = curNode.left;
            }
        }
        return results;
    }
}
