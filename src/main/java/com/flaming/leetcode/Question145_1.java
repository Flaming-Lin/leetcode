package com.flaming.leetcode;

import com.flaming.model.TreeNode;

import java.util.ArrayList;
import java.util.List;

public class Question145_1 extends Question145{

    @Override
    protected List<Integer> postorderTraversal(TreeNode root) {
        List<Integer> results = new ArrayList<>();
        postorderTraversal(root, results);
        return results;
    }

    private void postorderTraversal(TreeNode root, List<Integer> results) {
        if (null == root) {
            return;
        }
        postorderTraversal(root.left, results);
        postorderTraversal(root.right, results);
        results.add(root.val);
    }
}
