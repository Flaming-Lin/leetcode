package com.flaming.leetcode;

import com.flaming.model.TreeNode;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class Question145_2 extends Question145{

    @Override
    protected List<Integer> postorderTraversal(TreeNode root) {
        if (null == root) {
            return new ArrayList<>();
        }

        List<Integer> results = new ArrayList<>();
        Stack<TreeNode> stack = new Stack<>();
        TreeNode pre = root;
        while (!stack.isEmpty() || null != root) {
            while (null != root) {
                stack.push(root);
                root = root.left;
            }
            root = stack.pop();
            if (null == root.right || root.right == pre) {
                results.add(root.val);
                pre = root;
                root = null;
            } else {
                stack.push(root);
                root = root.right;
            }
        }
        return results;
    }
}
