package com.flaming.leetcode;

import com.flaming.AbstractLeetcodeQuestion;
import com.flaming.model.TreeNode;

//Implement an iterator over a binary search tree (BST). Your iterator will be i
//nitialized with the root node of a BST.
//
// Calling next() will return the next smallest number in the BST.
//
//
//
//
//
//
// Example:
//
//
//
//
//BSTIterator iterator = new BSTIterator(root);
//iterator.next();    // return 3
//iterator.next();    // return 7
//iterator.hasNext(); // return true
//iterator.next();    // return 9
//iterator.hasNext(); // return true
//iterator.next();    // return 15
//iterator.hasNext(); // return true
//iterator.next();    // return 20
//iterator.hasNext(); // return false
//
//
//
//
// Note:
//
//
// next() and hasNext() should run in average O(1) time and uses O(h) memory, wh
//ere h is the height of the tree.
// You may assume that next() call will always be valid, that is, there will be
//at least a next smallest number in the BST when next() is called.
//
// Related Topics 栈 树 设计
// 👍 284 👎 0

/**
 * Your BSTIterator object will be instantiated and called as such:
 * BSTIterator obj = new BSTIterator(root);
 * int param_1 = obj.next();
 * boolean param_2 = obj.hasNext();
 */
public abstract class Question173 extends AbstractLeetcodeQuestion {

    public Question173() {

    }

    public Question173(TreeNode root) {

    }

    /**
     * @return the next smallest number
     */
    public abstract int next();

    /**
     * @return whether we have a next smallest number
     */
    public abstract boolean hasNext();

    @Override
    protected void test() {

    }
}
