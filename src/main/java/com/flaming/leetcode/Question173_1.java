package com.flaming.leetcode;

import com.flaming.model.TreeNode;

import java.util.Stack;

public class Question173_1 extends Question173 {

    private Stack<TreeNode> stack = new Stack<>();

    public Question173_1() {
        super();
    }

    public Question173_1(TreeNode root) {
        super(root);
        while (null != root) {
            this.stack.push(root);
            root = root.left;
        }
    }

    @Override
    public int next() {
        TreeNode curNode = this.stack.pop();
        int result = curNode.val;
        curNode = curNode.right;
        while (null != curNode) {
            this.stack.push(curNode);
            curNode = curNode.left;
        }
        return result;
    }

    @Override
    public boolean hasNext() {
        return !this.stack.isEmpty();
    }

    @Override
    protected void test() {
        TreeNode root = new TreeNode(9,
                new TreeNode(3, null ,new TreeNode(7)),
                new TreeNode(20, new TreeNode(15), null));

        Question173 iterator = new Question173_1(root);
        System.out.println(iterator.next());    // return 3
        System.out.println(iterator.next());    // return 7
        System.out.println(iterator.hasNext()); // return true
        System.out.println(iterator.next());    // return 9
        System.out.println(iterator.hasNext()); // return true
        System.out.println(iterator.next());    // return 15
        System.out.println(iterator.hasNext()); // return true
        System.out.println(iterator.next());    // return 20
        System.out.println(iterator.hasNext()); // return false
    }
}
