package com.flaming.leetcode;

import com.flaming.AbstractLeetcodeQuestion;
import com.flaming.model.TreeNode;

import java.util.List;

//Given a binary tree, imagine yourself standing on the right side of it, return
// the values of the nodes you can see ordered from top to bottom.
//
// Example:
//
//
//Input: [1,2,3,null,5,null,4]
//Output: [1, 3, 4]
//Explanation:
//
//   1            <---
// /   \
//2     3         <---
// \     \
//  5     4       <---
// Related Topics 树 深度优先搜索 广度优先搜索
// 👍 359 👎 0
public abstract class Question199 extends AbstractLeetcodeQuestion {

    public abstract List<Integer> rightSideView(TreeNode root);

    @Override
    protected void test() {
        TreeNode root = new TreeNode(1,
                new TreeNode(2, null, new TreeNode(5)),
                new TreeNode(3, null, new TreeNode(4)));
        System.out.println(rightSideView(root));
    }
}
