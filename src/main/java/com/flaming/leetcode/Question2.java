package com.flaming.leetcode;

//You are given two non-empty linked lists representing two non-negative integer
//s. The digits are stored in reverse order, and each of their nodes contains a si
//ngle digit. Add the two numbers and return the sum as a linked list. 
//
// You may assume the two numbers do not contain any leading zero, except the nu
//mber 0 itself. 
//
// 
// Example 1: 
//
// 
//Input: l1 = [2,4,3], l2 = [5,6,4]
//Output: [7,0,8]
//Explanation: 342 + 465 = 807.
// 
//
// Example 2: 
//
// 
//Input: l1 = [0], l2 = [0]
//Output: [0]
// 
//
// Example 3: 
//
// 
//Input: l1 = [9,9,9,9,9,9,9], l2 = [9,9,9,9]
//Output: [8,9,9,9,0,0,0,1]
// 
//
// 
// Constraints: 
//
// 
// The number of nodes in each linked list is in the range [1, 100]. 
// 0 <= Node.val <= 9 
// It is guaranteed that the list represents a number that does not have leading
// zeros. 
// 
// Related Topics 链表 数学 
// 👍 5179 👎 0

import com.flaming.AbstractLeetcodeQuestion;
import com.flaming.model.ListNode;

public class Question2 extends AbstractLeetcodeQuestion {

    /**
     * 注意对于最后一个残余进位的处理
     */
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        if (null == l1 && null == l2) {
            return null;
        }

        ListNode dummyNode = new ListNode(0);
        ListNode curNode = dummyNode;
        boolean carry = false;
        while (null != l1 || null != l2) {
            int result = 0;
            if (null != l1) {
                result += l1.val;
                l1 = l1.next;
            }
            if (null != l2) {
                result += l2.val;
                l2 = l2.next;
            }
            result += carry ? 1 : 0;
            carry = result >= 10;
            curNode.next = new ListNode(result % 10);
            curNode = curNode.next;
        }
        if (carry) {
            curNode.next = new ListNode(1);
        }
        return dummyNode.next;
    }

    @Override
    public void test() {
        ListNode l1 = new ListNode(2);
        l1.next = new ListNode(4);
        l1.next.next = new ListNode(3);
        ListNode l2 = new ListNode(5);
        l2.next = new ListNode(6);
        l2.next.next = new ListNode(4);
        System.out.println(addTwoNumbers(l1, l2).toString());
    }
    
}
