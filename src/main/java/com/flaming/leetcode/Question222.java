package com.flaming.leetcode;

import com.flaming.AbstractLeetcodeQuestion;
import com.flaming.model.TreeNode;

//Given a complete binary tree, count the number of nodes.
//
// Note:
//
// Definition of a complete binary tree from Wikipedia:
//In a complete binary tree every level, except possibly the last, is completely
// filled, and all nodes in the last level are as far left as possible. It can hav
//e between 1 and 2h nodes inclusive at the last level h.
//
// Example:
//
//
//Input:
//    1
//   / \
//  2   3
// / \  /
//4  5 6
//
//Output: 6
// Related Topics 树 二分查找
// 👍 278 👎 0
public abstract class Question222 extends AbstractLeetcodeQuestion {

    protected abstract int countNodes(TreeNode root);

    @Override
    protected void test() {
        TreeNode root = new TreeNode(1,
                new TreeNode(2, new TreeNode(4), new TreeNode(5)),
                new TreeNode(3, new TreeNode(6), null));
        System.out.println(countNodes(root));
        root = new TreeNode(1);
        System.out.println(countNodes(root));
        root = TreeNode.generateCompleteBinaryTree(500);
        System.out.println(countNodes(root));
    }
}
