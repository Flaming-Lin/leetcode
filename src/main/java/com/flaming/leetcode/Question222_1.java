package com.flaming.leetcode;

import com.flaming.model.TreeNode;

public class Question222_1 extends Question222{

    @Override
    protected int countNodes(TreeNode root) {
        if (null == root) {
            return 0;
        }

        return countNodes(root.left) + countNodes(root.right) + 1;
    }

}
