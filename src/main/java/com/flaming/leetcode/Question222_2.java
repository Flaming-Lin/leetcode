package com.flaming.leetcode;

import com.flaming.model.TreeNode;

public class Question222_2 extends Question222{

    /**
     * 利用二分法来降低时间&空间复杂度。
     * - 首先计算树的高度 n，则前 n-1 层的节点共有 Math.pow(2, n-2) - 1 个
     * - 最后一层利用二分法，找到最右侧的叶子节点。最后一层共有 Math.pow(2, n-1) 个
     *   每次的分只与根节点相比较，若 index 小于中点 Math.pow(2, n-2)，则 root = root.left; 反之亦然。
     *
     * 空间复杂度 O(1) 时间复杂度 O(logD)
     */
    @Override
    protected int countNodes(TreeNode root) {
        if (null == root) {
            return 0;
        }

        int height = getHeight(root);
        int left = 1, right = (int)Math.pow(2, height - 1);
        while (left != right) {
            if (exist(root, height, (left + right + 1) / 2)) {
                left = (left + right + 1) / 2;
            } else {
                right = (left + right + 1) / 2 - 1;
            }
        }
        return (int)Math.pow(2, height - 1) - 1 + left;
    }

    private boolean exist(TreeNode root, int height, int index) {
        if (null == root) {
            return false;
        }
        if (height == 1) {
            return true;
        }
        int mid = (int)Math.pow(2, height - 2);
        if (index > mid) {
            return exist(root.right, height - 1, index - mid);
        } else {
            return exist(root.left, height - 1, index);
        }
    }

    private int getHeight(TreeNode root) {
        return null == root ? 0 : getHeight(root.left) + 1;
    }

}
