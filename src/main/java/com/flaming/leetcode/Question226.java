package com.flaming.leetcode;

import com.flaming.AbstractLeetcodeQuestion;
import com.flaming.model.TreeNode;

//Invert a binary tree.
//
// Example:
//
// Input:
//
//
//     4
//   /   \
//  2     7
// / \   / \
//1   3 6   9
//
// Output:
//
//
//     4
//   /   \
//  7     2
// / \   / \
//9   6 3   1
//
// Trivia:
//This problem was inspired by this original tweet by Max Howell:
//
// Google: 90% of our engineers use the software you wrote (Homebrew), but you c
//an’t invert a binary tree on a whiteboard so f*** off.
// Related Topics 树
// 👍 669 👎 0
public class Question226 extends AbstractLeetcodeQuestion {

    public TreeNode invertTree(TreeNode root) {
        if (null == root) {
            return null;
        }
        invertTree(root.left);
        invertTree(root.right);

        TreeNode tmpNode = root.right;
        root.right = root.left;
        root.left = tmpNode;
        return root;
    }

    @Override
    protected void test() {
        TreeNode treeNode = new TreeNode(4,
                new TreeNode(2, new TreeNode(1), new TreeNode(3)),
                new TreeNode(7, new TreeNode(6), new TreeNode(9)));
        System.out.println(invertTree(treeNode).toString());
    }
}
