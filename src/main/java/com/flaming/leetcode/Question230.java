package com.flaming.leetcode;

import com.flaming.AbstractLeetcodeQuestion;
import com.flaming.model.TreeNode;

//Given a binary search tree, write a function kthSmallest to find the kth small
//est element in it.
//
//
//
// Example 1:
//
//
//Input: root = [3,1,4,null,2], k = 1
//   3
//  / \
// 1   4
//  \
//   2
//Output: 1
//
// Example 2:
//
//
//Input: root = [5,3,6,2,4,null,null,1], k = 3
//       5
//      / \
//     3   6
//    / \
//   2   4
//  /
// 1
//Output: 3
//
//
// Follow up:
//What if the BST is modified (insert/delete operations) often and you need to f
//ind the kth smallest frequently? How would you optimize the kthSmallest routine?
//
//
//
// Constraints:
//
//
// The number of elements of the BST is between 1 to 10^4.
// You may assume k is always valid, 1 ≤ k ≤ BST's total elements.
//
// Related Topics 树 二分查找
// 👍 308 👎 0
public abstract class Question230 extends AbstractLeetcodeQuestion {

    public abstract int kthSmallest(TreeNode root, int k);

    @Override
    protected void test() {
        TreeNode root = new TreeNode(3,
                new TreeNode(1, null, new TreeNode(2)),
                new TreeNode(4));
        System.out.println(kthSmallest(root, 1));
    }
}
