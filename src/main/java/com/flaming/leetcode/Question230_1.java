package com.flaming.leetcode;

import com.flaming.model.TreeNode;

public class Question230_1 extends Question230 {

    private Integer index = 0;
    private Integer result = null;

    @Override
    public int kthSmallest(TreeNode root, int k) {
        index = k;
        inorderTraversal(root);
        return result;
    }

    private void inorderTraversal(TreeNode root) {
        if (null == root) {
            return;
        }
        inorderTraversal(root.left);
        if (null != result) {
            return;
        }
        if (index <= 1) {
            result = root.val;
            return;
        } else {
            this.index--;
        }
        inorderTraversal(root.right);
    }

    @Override
    protected void beforeTest() {
        super.beforeTest();
        this.index = 0;
        this.result = null;
    }
}
