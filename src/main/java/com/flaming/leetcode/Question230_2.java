package com.flaming.leetcode;

import com.flaming.model.TreeNode;

import java.util.Stack;

public class Question230_2 extends Question230 {

    @Override
    public int kthSmallest(TreeNode root, int k) {
        if (null == root) {
            return 0;
        }

        Stack<TreeNode> stack = new Stack<>();
        while (!stack.isEmpty() || null != root) {
            while (null != root) {
                stack.push(root);
                root = root.left;
            }
            root = stack.pop();
            if (k <= 1) {
                return root.val;
            } else {
                k--;
            }
            root = root.right;
        }
        return 0;
    }
}
