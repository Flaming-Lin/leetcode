package com.flaming.leetcode;

import com.flaming.model.TreeNode;

public class Question230_3 extends Question230 {

    @Override
    public int kthSmallest(TreeNode root, int k) {
        if (null == root) {
            return 0;
        }

        TreeNode curNode = root;
        TreeNode preNode;
        while (null != curNode) {
            preNode = curNode.left;
            if (null == preNode) {
                if (k <= 1) {
                    return curNode.val;
                }
                k--;
                curNode = curNode.right;
                continue;
            }
            while (null != preNode.right && curNode != preNode.right) {
                preNode = preNode.right;
            }
            if (curNode == preNode.right) {
                if (k <= 1) {
                    return curNode.val;
                }
                k--;
                preNode.right = null;
                curNode = curNode.right;
            } else {
                preNode.right = curNode;
                curNode = curNode.left;
            }
        }
        return 0;
    }
}
