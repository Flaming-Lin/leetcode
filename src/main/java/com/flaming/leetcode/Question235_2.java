package com.flaming.leetcode;

import com.flaming.model.TreeNode;

import java.util.ArrayList;
import java.util.List;

public class Question235_2 extends Question235{

    @Override
    protected TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        List<TreeNode> pPath = getPath(root, p);
        List<TreeNode> qPath = getPath(root, q);
        if (pPath.isEmpty() || qPath.isEmpty()) {
            return null;
        }
        TreeNode result = null;
        for (int i = 0; i < Math.min(pPath.size(), qPath.size()); i++) {
            if (pPath.get(i) == qPath.get(i)) {
                result = pPath.get(i);
            } else {
                break;
            }
        }
        return result;
    }

    private List<TreeNode> getPath(TreeNode root, TreeNode target) {
        if (null == root || null == target) {
            return new ArrayList<>();
        }

        List<TreeNode> path = new ArrayList<>();
        while (root != target) {
            path.add(root);
            if (root.val < target.val) {
                root = root.right;
            } else {
                root = root.left;
            }
        }
        path.add(root);
        return path;
    }
}
