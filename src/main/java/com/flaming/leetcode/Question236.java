package com.flaming.leetcode;

import com.flaming.AbstractLeetcodeQuestion;
import com.flaming.model.TreeNode;

public abstract class Question236 extends AbstractLeetcodeQuestion {

    protected abstract TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q);

    @Override
    protected void test() {
        TreeNode leftNode = new TreeNode(2,
                new TreeNode(0),
                new TreeNode(4, new TreeNode(3), new TreeNode(5)));
        TreeNode rightNode = new TreeNode(8,
                new TreeNode(7),
                new TreeNode(9));
        TreeNode treeNode = new TreeNode(6, leftNode, rightNode);
        System.out.println(lowestCommonAncestor(treeNode, leftNode, rightNode));
    }

}
