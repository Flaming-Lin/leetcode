package com.flaming.leetcode;

import com.flaming.model.TreeNode;

public class Question236_1 extends Question236{

    @Override
    protected TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        if (null == root) {
            return null;
        }
        if (root == p || root == q) {
            return root;
        }
        TreeNode leftResult = lowestCommonAncestor(root.left, p, q);
        TreeNode rightResult = lowestCommonAncestor(root.right, p, q);
        if (null != leftResult && null != rightResult) {
            return root;
        } else if (null != leftResult) {
            return leftResult;
        } else {
            return rightResult;
        }
    }
}
