package com.flaming.leetcode;

import com.flaming.model.TreeNode;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Question236_2 extends Question236{

    @Override
    protected TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        Map<Integer, TreeNode> parentMap = new HashMap<>();
        fillParentMap(root, parentMap);
        Set<Integer> visited = new HashSet<>();
        while (p != null) {
            visited.add(p.val);
            p = parentMap.get(p.val);
        }
        while (q != null) {
            if (visited.contains(q.val)) {
                return q;
            }
            q = parentMap.get(q.val);
        }
        return null;
    }

    private void fillParentMap(TreeNode root, Map<Integer, TreeNode> parentMap) {
        if (null != root.left) {
            parentMap.put(root.left.val, root);
            fillParentMap(root.left, parentMap);
        }
        if (null != root.right) {
            parentMap.put(root.right.val, root);
            fillParentMap(root.right, parentMap);
        }
    }
}
