package com.flaming.leetcode;

import com.flaming.AbstractLeetcodeQuestion;
import com.flaming.model.TreeNode;

import java.util.List;

//Given a binary tree, return all root-to-leaf paths.
//
// Note: A leaf is a node with no children.
//
// Example:
//
//
//Input:
//
//   1
// /   \
//2     3
// \
//  5
//
//Output: ["1->2->5", "1->3"]
//
//Explanation: All root-to-leaf paths are: 1->2->5, 1->3
// Related Topics 树 深度优先搜索
// 👍 393 👎 0
public abstract class Question257 extends AbstractLeetcodeQuestion {

    protected abstract List<String> binaryTreePaths(TreeNode root);

    @Override
    protected void test() {
        TreeNode treeNode = new TreeNode(1,
                new TreeNode(2, null, new TreeNode(5)),
                new TreeNode(3));
        System.out.println(binaryTreePaths(treeNode));
    }
}
