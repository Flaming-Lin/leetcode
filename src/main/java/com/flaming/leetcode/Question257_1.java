package com.flaming.leetcode;

import com.flaming.model.TreeNode;

import java.util.ArrayList;
import java.util.List;

public class Question257_1 extends Question257{

    @Override
    protected List<String> binaryTreePaths(TreeNode root) {
        List<String> results = new ArrayList<>();
        binaryTreePaths(root, "", results);
        return results;
    }

    private void binaryTreePaths(TreeNode root, String pre, List<String> results) {
        if (null == root) {
            return;
        }
        pre = pre.isEmpty() ? pre + root.val : pre + "->" + root.val;
        if (null == root.left && null == root.right) {
            results.add(pre);
            return;
        }
        binaryTreePaths(root.left, pre, results);
        binaryTreePaths(root.right, pre, results);
    }
}
