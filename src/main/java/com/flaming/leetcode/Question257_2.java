package com.flaming.leetcode;

import com.flaming.model.TreeNode;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class Question257_2 extends Question257{

    @Override
    protected List<String> binaryTreePaths(TreeNode root) {
        if (null == root) {
            return new ArrayList<>();
        }
        List<String> results = new ArrayList<>();
        Stack<TreeNode> stack = new Stack<>();
        Stack<String> strStack = new Stack<>();
        stack.push(root);
        strStack.push("" + root.val);
        while (!stack.isEmpty()) {
            TreeNode curNode = stack.pop();
            String path = strStack.pop();
            if (null == curNode.left && null == curNode.right) {
                results.add(path);
                continue;
            }
            if (null != curNode.right) {
                stack.push(curNode.right);
                strStack.push(path + "->" + curNode.right.val);
            }
            if (null != curNode.left) {
                stack.push(curNode.left);
                strStack.push(path + "->" + curNode.left.val);
            }
        }
        return results;
    }
}
