package com.flaming.leetcode;

import com.flaming.AbstractLeetcodeQuestion;
import com.flaming.model.TreeNode;

import java.util.*;

//Serialization is the process of converting a data structure or object into a s
//equence of bits so that it can be stored in a file or memory buffer, or transmit
//ted across a network connection link to be reconstructed later in the same or an
//other computer environment.
//
// Design an algorithm to serialize and deserialize a binary tree. There is no r
//estriction on how your serialization/deserialization algorithm should work. You
//just need to ensure that a binary tree can be serialized to a string and this st
//ring can be deserialized to the original tree structure.
//
// Clarification: The input/output format is the same as how LeetCode serializes
// a binary tree. You do not necessarily need to follow this format, so please be
//creative and come up with different approaches yourself.
//
//
// Example 1:
//
//
//Input: root = [1,2,3,null,null,4,5]
//Output: [1,2,3,null,null,4,5]
//
//
// Example 2:
//
//
//Input: root = []
//Output: []
//
//
// Example 3:
//
//
//Input: root = [1]
//Output: [1]
//
//
// Example 4:
//
//
//Input: root = [1,2]
//Output: [1,2]
//
//
//
// Constraints:
//
//
// The number of nodes in the tree is in the range [0, 104].
// -1000 <= Node.val <= 1000
//
// Related Topics 树 设计
// 👍 481 👎 0
public class Question297 extends AbstractLeetcodeQuestion {

    // Encodes a tree to a single string.
    public String serialize(TreeNode root) {
        List<String> result = new ArrayList<>();

        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);
        while (!queue.isEmpty()) {
            TreeNode curNode = queue.poll();
            if (null == curNode) {
                result.add("#");
                continue;

            }
            result.add(String.valueOf(curNode.val));
            queue.offer(curNode.left);
            queue.offer(curNode.right);
        }

        return String.join(",", result);
    }

    // Decodes your encoded data to tree.
    public TreeNode deserialize(String data) {
        if (null == data || data.isEmpty()) {
            return null;
        }
        List<String> elements = Arrays.asList(data.split(","));
        if ("#".equals(elements.get(0))) {
            return null;
        }

        Queue<TreeNode> queue = new LinkedList<>();
        TreeNode root = new TreeNode(Integer.parseInt(elements.get(0)));
        queue.offer(root);
        for (int i = 1; i < elements.size(); i+=2) {
            TreeNode curNode = queue.poll();
            if ("#".equals(elements.get(i))) {
                curNode.left = null;
            } else {
                curNode.left = new TreeNode(Integer.parseInt(elements.get(i)));
                queue.offer(curNode.left);
            }
            if ("#".equals(elements.get(i + 1))) {
                curNode.right = null;
            } else {
                curNode.right = new TreeNode(Integer.parseInt(elements.get(i + 1)));
                queue.offer(curNode.right);
            }
        }
        return root;
    }

    @Override
    protected void test() {
        TreeNode root = new TreeNode(1, new TreeNode(2), new TreeNode(3, new TreeNode(4), new TreeNode(5)));
        String string = serialize(root);
        System.out.println(string);
        System.out.println(deserialize(string));
        root = null;
        string = serialize(root);
        System.out.println(string);
        System.out.println(deserialize(string));
    }
}
