package com.flaming.leetcode;

import com.flaming.AbstractLeetcodeQuestion;

import java.util.Arrays;

//Given a string s, find the length of the longest substring without repeating c
//haracters.
//
//
// Example 1:
//
//
//Input: s = "abcabcbb"
//Output: 3
//Explanation: The answer is "abc", with the length of 3.
//
//
// Example 2:
//
//
//Input: s = "bbbbb"
//Output: 1
//Explanation: The answer is "b", with the length of 1.
//
//
// Example 3:
//
//
//Input: s = "pwwkew"
//Output: 3
//Explanation: The answer is "wke", with the length of 3.
//Notice that the answer must be a substring, "pwke" is a subsequence and not a
//substring.
//
//
// Example 4:
//
//
//Input: s = ""
//Output: 0
//
//
//
// Constraints:
//
//
// 0 <= s.length <= 5 * 104
// s consists of English letters, digits, symbols and spaces.
//
// Related Topics 哈希表 双指针 字符串 Sliding Window
// 👍 4528 👎 0
public class Question3 extends AbstractLeetcodeQuestion {

    public int lengthOfLongestSubstring(String s) {
        if (null == s || s.isEmpty()) {
            return 0;
        }

        int[] charMap = new int[128];
        Arrays.fill(charMap, -1);
        int maxLength = 0;
        for (int left = -1, right = 0; right < s.length(); right++) {
            left = Math.max(left, charMap[s.charAt(right)]);
            maxLength = Math.max(maxLength, right - left);
            charMap[s.charAt(right)] = right;
        }
        return maxLength;
    }

    @Override
    public void test() {
        System.out.println(lengthOfLongestSubstring("abcabcbb"));
    }

}
