package com.flaming.leetcode;

import com.flaming.AbstractLeetcodeQuestion;

//One way to serialize a binary tree is to use preorder traversal. When we
//encounter a non-null node, we record the node's value. If it is a null node, we
//record using a sentinel value such as '#'.
//
// For example, the above binary tree can be serialized to the string "9,3,4,#,#
//,1,#,#,2,#,6,#,#", where '#' represents a null node.
//
// Given a string of comma-separated values preorder, return true if it is a
//correct preorder traversal serialization of a binary tree.
//
// It is guaranteed that each comma-separated value in the string must be
//either an integer or a character '#' representing null pointer.
//
// You may assume that the input format is always valid.
//
//
// For example, it could never contain two consecutive commas, such as "1,,3".
//
//
// Note: You are not allowed to reconstruct the tree.
//
//
// Example 1:
// Input: preorder = "9,3,4,#,#,1,#,#,2,#,6,#,#"
//Output: true
// Example 2:
// Input: preorder = "1,#"
//Output: false
// Example 3:
// Input: preorder = "9,#,#,1"
//Output: false
//
//
// Constraints:
//
//
// 1 <= preorder.length <= 10⁴
// preorder consist of integers in the range [0, 100] and '#' separated by
//commas ','.
//
// Related Topics 栈 树 字符串 二叉树 👍 357 👎 0
public class Question331 extends AbstractLeetcodeQuestion {

    /**
     * 背景知识：
     *
     * 入度：有多少个节点指向它；
     * 出度：它指向多少个节点。
     * 我们知道在树（甚至图）中，所有节点的入度之和等于出度之和。可以根据这个特点判断输入序列是否为有效的！
     *
     * 在一棵二叉树中：
     *
     * 每个空节点（ "#" ）会提供 0 个出度和 1 个入度。
     * 每个非空节点会提供 2 个出度和 1 个入度（根节点的入度是 0）。
     */
    public boolean isValidSerialization(String preorder) {
        if (null == preorder || preorder.isEmpty()) {
            return false;
        }

        String[] strArr = preorder.split(",");
        Integer[] arr = new Integer[strArr.length];
        for (int i = 0; i < strArr.length; i++) {
            if ("#".equals(strArr[i])) {
                arr[i] = null;
            } else {
                arr[i] = Integer.parseInt(strArr[i]);
            }
        }

        int diff = 1;
        for (Integer node : arr) {
            diff -= 1;
            if (diff < 0) {
                return false;
            }
            if (null != node) {
                diff += 2;
            }
        }
        return diff == 0;
    }

    @Override
    protected void test() {
        String preOrder = "9,3,4,#,#,1,#,#,2,#,6,#,#";
        System.out.println(isValidSerialization(preOrder));
        preOrder = "1";
        System.out.println(isValidSerialization(preOrder));
        preOrder = "1,#";
        System.out.println(isValidSerialization(preOrder));
        preOrder = "9,#,#,1";
        System.out.println(isValidSerialization(preOrder));
        preOrder = "#,7,6,9,#,#,#";
        System.out.println(isValidSerialization(preOrder));
    }
}
