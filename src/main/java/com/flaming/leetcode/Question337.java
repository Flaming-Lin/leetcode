package com.flaming.leetcode;

import com.flaming.AbstractLeetcodeQuestion;
import com.flaming.model.TreeNode;

import java.util.HashMap;
import java.util.Map;

//The thief has found himself a new place for his thievery again. There is only
//one entrance to this area, called the "root." Besides the root, each house has o
//ne and only one parent house. After a tour, the smart thief realized that "all h
//ouses in this place forms a binary tree". It will automatically contact the poli
//ce if two directly-linked houses were broken into on the same night.
//
// Determine the maximum amount of money the thief can rob tonight without alert
//ing the police.
//
// Example 1:
//
//
//Input: [3,2,3,null,3,null,1]
//
//     3
//    / \
//   2   3
//    \   \
//     3   1
//
//Output: 7
//Explanation: Maximum amount of money the thief can rob = 3 + 3 + 1 = 7.
//
// Example 2:
//
//
//Input: [3,4,5,1,3,null,1]
//
//     3
//    / \
//   4   5
//  / \   \
// 1   3   1
//
//Output: 9
//Explanation: Maximum amount of money the thief can rob = 4 + 5 = 9.
// Related Topics 树 深度优先搜索
// 👍 645 👎 0
public class Question337 extends AbstractLeetcodeQuestion {

    public int rob(TreeNode root) {
        Map<TreeNode, Integer> robMap = new HashMap<>();
        Map<TreeNode, Integer> noRobMap = new HashMap<>();
        traversal(root, robMap, noRobMap);
        return Math.max(robMap.getOrDefault(root, 0), noRobMap.getOrDefault(root, 0));
    }

    private void traversal(TreeNode root, Map<TreeNode, Integer> robMap, Map<TreeNode, Integer> noRobMap) {
        if (null == root) {
            return;
        }

        traversal(root.left, robMap, noRobMap);
        traversal(root.right, robMap, noRobMap);
        robMap.put(root, root.val + noRobMap.getOrDefault(root.left, 0) + noRobMap.getOrDefault(root.right, 0));
        noRobMap.put(root, Math.max(robMap.getOrDefault(root.left, 0), noRobMap.getOrDefault(root.left, 0)) +
                Math.max(robMap.getOrDefault(root.right, 0) , noRobMap.getOrDefault(root.right, 0)));
    }

    @Override
    protected void test() {
        TreeNode root = new TreeNode(3,
                new TreeNode(4, new TreeNode(1), new TreeNode(3)),
                new TreeNode(5, null, new TreeNode(1)));
        System.out.println(rob(root));
    }
}
