package com.flaming.leetcode;

import com.flaming.AbstractLeetcodeQuestion;

import java.util.*;

//You are given a nested list of integers nestedList. Each element is either an
//integer or a list whose elements may also be integers or other lists. Implement
//an iterator to flatten it.
//
// Implement the NestedIterator class:
//
//
// NestedIterator(List<NestedInteger> nestedList) Initializes the iterator with
//the nested list nestedList.
// int next() Returns the next integer in the nested list.
// boolean hasNext() Returns true if there are still some integers in the
//nested list and false otherwise.
//
//
// Your code will be tested with the following pseudocode:
//
//
//initialize iterator with nestedList
//res = []
//while iterator.hasNext()
//    append iterator.next() to the end of res
//return res
//
//
// If res matches the expected flattened list, then your code will be judged as
//correct.
//
//
// Example 1:
//
//
//Input: nestedList = [[1,1],2,[1,1]]
//Output: [1,1,2,1,1]
//Explanation: By calling next repeatedly until hasNext returns false, the
//order of elements returned by next should be: [1,1,2,1,1].
//
//
// Example 2:
//
//
//Input: nestedList = [1,[4,[6]]]
//Output: [1,4,6]
//Explanation: By calling next repeatedly until hasNext returns false, the
//order of elements returned by next should be: [1,4,6].
//
//
//
// Constraints:
//
//
// 1 <= nestedList.length <= 500
// The values of the integers in the nested list is in the range [-10⁶, 10⁶].
//
// Related Topics 栈 树 深度优先搜索 设计 队列 迭代器 👍 402 👎 0


//leetcode submit region begin(Prohibit modification and deletion)
public class Question341 extends AbstractLeetcodeQuestion implements Iterator<Integer> {

    private final Stack<NestedInteger> stack = new Stack<>();

    /**
     * 这题实现了 Iterator，暂时不能 newInstance 测试，直接跑 main 吧
     *
     * 题目看着很简单，就是一个类似多叉树的拍平，关键是「不能在初始化的时候直接拍完」，太浪费空间
     * 正确的做法是在 next() 和 hasNext() 时进行局部拍平
     *
     * 所以用 Stack 倒序把每层元素压进来，用到的时候再取
     */
    public Question341(List<NestedInteger> nestedList) {
        pushStack(nestedList);
    }

    @Override
    public Integer next() {
        NestedInteger node = this.stack.pop();
        if (null == node || !node.isInteger()) {
            return null;
        }
        return node.getInteger();
    }

    @Override
    public boolean hasNext() {
        while (true) {
            if (this.stack.isEmpty()) {
                return false;
            }
            NestedInteger node = this.stack.peek();
            if (null == node) {
                return false;
            }
            if (node.isInteger()) {
                return true;
            } else {
                pushStack(this.stack.pop().getList());
            }
        }
    }

    private void pushStack(List<NestedInteger> nestedList) {
        if (null == nestedList || nestedList.isEmpty()) {
            return;
        }
        for (int i = nestedList.size() - 1; i >= 0; i--) {
            stack.push(nestedList.get(i));
        }
    }

    public static void main(String[] args) {
        Question341 root = new Question341(Arrays.asList(
                new DefaultNestedInteger(
                        Arrays.asList(new DefaultNestedInteger(1), new DefaultNestedInteger(1))
                ),
                new DefaultNestedInteger(2),
                new DefaultNestedInteger(
                        Arrays.asList(new DefaultNestedInteger(1), new DefaultNestedInteger(1))
                )
        ));

        while (root.hasNext()) {
            System.out.print(root.next() + " ");
        }
    }

    @Override
    protected void test() {
        Question341 root = new Question341(Arrays.asList(
                new DefaultNestedInteger(
                        Arrays.asList(new DefaultNestedInteger(1), new DefaultNestedInteger(1))
                ),
                new DefaultNestedInteger(2),
                new DefaultNestedInteger(
                        Arrays.asList(new DefaultNestedInteger(1), new DefaultNestedInteger(1))
                )
        ));

        while (root.hasNext()) {
            System.out.print(root.next() + " ");
        }

    }

    interface NestedInteger {
        // @return true if this NestedInteger holds a single integer, rather than a nested list.
        boolean isInteger();

        // @return the single integer that this NestedInteger holds, if it holds a single integer
        // Return null if this NestedInteger holds a nested list
        Integer getInteger();

        // @return the nested list that this NestedInteger holds, if it holds a nested list
        // Return empty list if this NestedInteger holds a single integer
        List<NestedInteger> getList();
    }

    static class DefaultNestedInteger implements NestedInteger {

        private Integer value;

        private List<NestedInteger> list;

        public DefaultNestedInteger(Integer value) {
            this.value = value;
        }

        public DefaultNestedInteger(List<NestedInteger> list) {
            this.list = list;
        }

        @Override
        public boolean isInteger() {
            return null != value;
        }

        @Override
        public Integer getInteger() {
            return this.value;
        }

        @Override
        public List<NestedInteger> getList() {
            return this.list;
        }
    }

}
