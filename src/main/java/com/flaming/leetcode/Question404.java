package com.flaming.leetcode;

import com.flaming.AbstractLeetcodeQuestion;
import com.flaming.model.TreeNode;

//Find the sum of all left leaves in a given binary tree.
//
// Example:
//
//    3
//   / \
//  9  20
//    /  \
//   15   7
//
//There are two left leaves in the binary tree, with values 9 and 15 respectivel
//y. Return 24.
//
// Related Topics 树
// 👍 257 👎 0
public class Question404 extends AbstractLeetcodeQuestion {

    public int sumOfLeftLeaves(TreeNode root) {
        if (null == root) {
            return 0;
        }

        int cur = 0;
        if (null != root.left && null == root.left.left && null == root.left.right) {
            cur += root.left.val;
        }
        return cur + sumOfLeftLeaves(root.left) + sumOfLeftLeaves(root.right);
    }

    @Override
    protected void test() {
        TreeNode treeNode = new TreeNode(3,
                new TreeNode(9),
                new TreeNode(20, new TreeNode(15), new TreeNode(7)));
        System.out.println(sumOfLeftLeaves(treeNode));
        treeNode = new TreeNode(1, null, new TreeNode(2));
        System.out.println(sumOfLeftLeaves(treeNode));
    }
}
