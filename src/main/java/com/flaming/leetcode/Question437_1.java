package com.flaming.leetcode;

import com.flaming.model.TreeNode;

import java.util.Stack;

public class Question437_1 extends Question437 {

    private int result = 0;

    @Override
    protected int pathSum(TreeNode root, int sum) {
        if (null == root) {
            return 0;
        }

        Stack<TreeNode> stack = new Stack<>();
        preorder(root, stack, sum);
        return this.result;
    }

    private void preorder(TreeNode root, Stack<TreeNode> stack, int sum) {
        if (null == root) {
            return;
        }

        stack.push(root);
        int total = 0;
        for (int i = stack.size() - 1; i >= 0; i--) {
            total += stack.get(i).val;
            if (total == sum) {
                this.result++;
            }
        }
        preorder(root.left, stack, sum);
        preorder(root.right, stack, sum);
        stack.pop();
    }

    @Override
    protected void beforeTest() {
        super.beforeTest();
        this.result = 0;
    }
}
