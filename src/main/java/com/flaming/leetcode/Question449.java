package com.flaming.leetcode;

import com.flaming.AbstractLeetcodeQuestion;
import com.flaming.model.TreeNode;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

//Serialization is converting a data structure or object into a sequence of bits
// so that it can be stored in a file or memory buffer, or transmitted across a ne
//twork connection link to be reconstructed later in the same or another computer
//environment.
//
// Design an algorithm to serialize and deserialize a binary search tree. There
//is no restriction on how your serialization/deserialization algorithm should wor
//k. You need to ensure that a binary search tree can be serialized to a string, a
//nd this string can be deserialized to the original tree structure.
//
// The encoded string should be as compact as possible.
//
//
// Example 1:
// Input: root = [2,1,3]
//Output: [2,1,3]
// Example 2:
// Input: root = []
//Output: []
//
//
// Constraints:
//
//
// The number of nodes in the tree is in the range [0, 104].
// 0 <= Node.val <= 104
// The input tree is guaranteed to be a binary search tree.
//
// Related Topics 树
// 👍 157 👎 0
public class Question449 extends AbstractLeetcodeQuestion {

    // Encodes a tree to a single string.
    public String serialize(TreeNode root) {
        StringBuilder postOrder = new StringBuilder();

        // postOrder
        Stack<TreeNode> stack = new Stack<>();
        TreeNode preNode = null;
        while (null != root || !stack.isEmpty()) {
            while (null != root) {
                stack.push(root);
                root = root.left;
            }
            root = stack.peek();
            if (null != root.right && preNode != root.right) {
                root = root.right;
                continue;
            }
            root = stack.pop();
            postOrder.append((char) root.val);
            preNode = root;
            root = null;
        }

        return postOrder.toString();
    }

    // Decodes your encoded data to tree.
    public TreeNode deserialize(String data) {
        if (null == data || data.isEmpty()) {
            return null;
        }

        rootIndex = data.length();
        return build(data, Integer.MIN_VALUE, Integer.MAX_VALUE);
    }

    private int rootIndex;

    private TreeNode build(String data, int min, int max) {
        if (rootIndex <= 0) {
            return null;
        }
        int value = data.charAt(rootIndex - 1);
        if (value < min || value > max) {
            return null;
        }
        rootIndex--;

        TreeNode root = new TreeNode(value);
        root.right = build(data, value, max);
        root.left = build(data, min, value);
        return root;
    }

    @Override
    protected void test() {
        TreeNode root = new TreeNode(1, null, new TreeNode(2, new TreeNode(2), null));
        System.out.println(deserialize(serialize(root)));
        this.beforeTest();
        root = new TreeNode(2, new TreeNode(1), new TreeNode(3));
        System.out.println(deserialize(serialize(root)));
    }
}
