package com.flaming.leetcode;

import com.flaming.AbstractLeetcodeQuestion;
import com.flaming.model.TreeNode;

//Given a root node reference of a BST and a key, delete the node with the given
// key in the BST. Return the root node reference (possibly updated) of the BST.
//
// Basically, the deletion can be divided into two stages:
//
//
// Search for a node to remove.
// If the node is found, delete the node.
//
//
// Follow up: Can you solve it with time complexity O(height of tree)?
//
//
// Example 1:
//
//
//Input: root = [5,3,6,2,4,null,7], key = 3
//Output: [5,4,6,2,null,null,7]
//Explanation: Given key to delete is 3. So we find the node with value 3 and de
//lete it.
//One valid answer is [5,4,6,2,null,null,7], shown in the above BST.
//Please notice that another valid answer is [5,2,6,null,4,null,7] and it's also
// accepted.
//
//
//
// Example 2:
//
//
//Input: root = [5,3,6,2,4,null,7], key = 0
//Output: [5,3,6,2,4,null,7]
//Explanation: The tree does not contain a node with value = 0.
//
//
// Example 3:
//
//
//Input: root = [], key = 0
//Output: []
//
//
//
// Constraints:
//
//
// The number of nodes in the tree is in the range [0, 104].
// -105 <= Node.val <= 105
// Each node has a unique value.
// root is a valid binary search tree.
// -105 <= key <= 105
//
// Related Topics 树
// 👍 393 👎 0
public class Question450 extends AbstractLeetcodeQuestion {

    /**
     * 这道题还是借助「递归」来进行任务拆解简化。
     *
     * 找到目标节点后分三种情况：
     * - 目标节点为叶子节点，直接删除。
     * - 目标节点左节点非空，互相置换左节点的最右叶子的值，递归删除左节点下的目标值。
     * - 反之亦然。
     */
    public TreeNode deleteNode(TreeNode root, int key) {
        if (null == root) {
            return null;
        }

        if (key > root.val) {
            root.right = deleteNode(root.right, key);
        } else if (key < root.val) {
            root.left = deleteNode(root.left, key);
        } else {
            if (null == root.left && null == root.right) {
                return null;
            }
            TreeNode tmpNode;
            if (null != root.left) {
                tmpNode = root.left;
                while (null != tmpNode.right) {
                    tmpNode = tmpNode.right;
                }
                root.val = tmpNode.val;
                tmpNode.val = key;
                root.left = deleteNode(root.left, key);
            } else {
                tmpNode = root.right;
                while (null != tmpNode.left) {
                    tmpNode = tmpNode.left;
                }
                root.val = tmpNode.val;
                tmpNode.val = key;
                root.right = deleteNode(root.right, key);
            }
        }
        return root;
    }

    @Override
    protected void test() {
        TreeNode root = new TreeNode(5,
                new TreeNode(3, new TreeNode(2), new TreeNode(4)),
                new TreeNode(6, null, new TreeNode(7)));

        System.out.println(deleteNode(root, 3));
    }
}
