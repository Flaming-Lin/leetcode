package com.flaming.leetcode;

import com.flaming.AbstractLeetcodeQuestion;

//Given a string s, return the longest palindromic substring in s.
//
//
// Example 1:
//
//
//Input: s = "babad"
//Output: "bab"
//Note: "aba" is also a valid answer.
//
//
// Example 2:
//
//
//Input: s = "cbbd"
//Output: "bb"
//
//
// Example 3:
//
//
//Input: s = "a"
//Output: "a"
//
//
// Example 4:
//
//
//Input: s = "ac"
//Output: "a"
//
//
//
// Constraints:
//
//
// 1 <= s.length <= 1000
// s consist of only digits and English letters (lower-case and/or upper-case),
//
//
// Related Topics 字符串 动态规划
// 👍 2855 👎 0
public class Question5 extends AbstractLeetcodeQuestion {

    public String longestPalindrome(String s) {
        if (null == s || s.isEmpty()) {
            return "";
        }

        String maxResult = "";
        for (int i = 0; i < s.length(); i++) {
            String result1 = getPalindrome(s, i, i);
            String result2 = getPalindrome(s, i, i + 1);
            String curResult = result1.length() > result2.length() ? result1 : result2;
            maxResult = curResult.length() > maxResult.length() ? curResult : maxResult;
        }
        return maxResult;
    }

    private String getPalindrome(String s, int left, int right) {
        if (null == s || s.isEmpty()) {
            return "";
        }

        while (left >= 0 && right < s.length() && s.charAt(left) == s.charAt(right)) {
            left--;
            right++;
        }
        return s.substring(left + 1, right);
    }

    @Override
    protected void test() {
        System.out.println(longestPalindrome("babad"));
    }
}
