package com.flaming.leetcode;

import com.flaming.AbstractLeetcodeQuestion;
import com.flaming.model.TreeNode;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

//Given a binary search tree (BST) with duplicates, find all the mode(s) (the mo
//st frequently occurred element) in the given BST.
//
// Assume a BST is defined as follows:
//
//
// The left subtree of a node contains only nodes with keys less than or equal t
//o the node's key.
// The right subtree of a node contains only nodes with keys greater than or equ
//al to the node's key.
// Both the left and right subtrees must also be binary search trees.
//
//
//
//
// For example:
//Given BST [1,null,2,2],
//
//
//   1
//    \
//     2
//    /
//   2
//
//
//
//
// return [2].
//
// Note: If a tree has more than one mode, you can return them in any order.
//
// Follow up: Could you do that without using any extra space? (Assume that the
//implicit stack space incurred due to recursion does not count).
// Related Topics 树
// 👍 243 👎 0
public class Question501 extends AbstractLeetcodeQuestion {

    private int maxTime = 0;
    private int curTime = 0;
    private Integer lastValue = null;

    public int[] findMode(TreeNode root) {
        if (null == root) {
            return new int[]{};
        }

        Set<Integer> results = new HashSet<>();
        TreeNode curNode = root;
        TreeNode preNode = null;
        while (null != curNode) {
            preNode = curNode.left;
            if (null == preNode) {
                visit(curNode, results);
                curNode = curNode.right;
                continue;
            }
            while (null != preNode.right && curNode != preNode.right) {
                preNode = preNode.right;
            }
            if (null == preNode.right) {
                preNode.right = curNode;
                curNode = curNode.left;
                continue;
            }
            preNode.right = null;
            visit(curNode, results);
            curNode = curNode.right;
        }

        return results.stream().mapToInt(Integer::valueOf).toArray();
    }

    private void visit(TreeNode curNode, Set<Integer> results) {
        if (null == curNode) {
            return;
        }

        curTime = (null == lastValue || curNode.val == lastValue) ? curTime + 1 : 1;

        if (curTime == maxTime) {
            results.add(curNode.val);
        } else if (curTime > maxTime) {
            results.clear();
            results.add(curNode.val);
            maxTime = curTime;
        }

        lastValue = curNode.val;
    }

    @Override
    protected void beforeTest() {
        super.beforeTest();
        this.maxTime = 0;
        this.curTime = 0;
        this.lastValue = 0;
    }

    @Override
    protected void test() {
        TreeNode root = new TreeNode(1, null, new TreeNode(2, new TreeNode(2), null));
        int[] result = findMode(root);
        for (int i = 0; i < result.length; i++) {
            System.out.print(result[i] + " ");
        }
        System.out.println();
        this.beforeTest();
        root = new TreeNode(1, null, new TreeNode(2));
        result = findMode(root);
        for (int i = 0; i < result.length; i++) {
            System.out.print(result[i] + " ");
        }
    }
}
