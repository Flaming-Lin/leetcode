package com.flaming.leetcode;

import com.flaming.AbstractLeetcodeQuestion;
import com.flaming.model.TreeNode;

import java.util.*;

//Given the root of a binary tree, return the most frequent subtree sum. If
//there is a tie, return all the values with the highest frequency in any order.
//
// The subtree sum of a node is defined as the sum of all the node values
//formed by the subtree rooted at that node (including the node itself).
//
//
// Example 1:
//
//
//Input: root = [5,2,-3]
//Output: [2,-3,4]
//
//
// Example 2:
//
//
//Input: root = [5,2,-5]
//Output: [2]
//
//
//
// Constraints:
//
//
// The number of nodes in the tree is in the range [1, 10⁴].
// -10⁵ <= Node.val <= 10⁵
//
// Related Topics 树 深度优先搜索 哈希表 二叉树 👍 137 👎 0


//leetcode submit region begin(Prohibit modification and deletion)
public class Question508 extends AbstractLeetcodeQuestion {

    private Map<Integer, Integer> occurTimeMap = new HashMap<>(16);

    public int[] findFrequentTreeSum(TreeNode root) {
        postOrderTraversal(root);

        List<Integer> results = new ArrayList<>();
        int maxTime = 0;
        for (Map.Entry<Integer, Integer> entry : this.occurTimeMap.entrySet()) {
            int value = entry.getKey();
            int occurTime = entry.getValue();
            if (occurTime > maxTime) {
                maxTime = occurTime;
                results.clear();
                results.add(value);
            } else if (occurTime == maxTime) {
                results.add(value);
            }
        }

        int[] resultArr = new int[results.size()];
        for (int i = 0; i < results.size(); i++) {
            resultArr[i] = results.get(i);
        }
        return resultArr;
    }

    private int postOrderTraversal(TreeNode root) {
        if (null == root) {
            return 0;
        }

        int left = postOrderTraversal(root.left);
        int right = postOrderTraversal(root.right);
        int total = root.val + left + right;
        int occurTime = this.occurTimeMap.getOrDefault(total, 0) + 1;
        this.occurTimeMap.put(total, occurTime);
        return total;
    }

    @Override
    protected void test() {
        TreeNode root = new TreeNode(5, new TreeNode(2), new TreeNode(-3));
        System.out.println(Arrays.toString(findFrequentTreeSum(root)));
        root = new TreeNode(5, new TreeNode(2), new TreeNode(-5));
        System.out.println(Arrays.toString(findFrequentTreeSum(root)));
    }
}
