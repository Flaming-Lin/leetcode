package com.flaming.leetcode;

import com.flaming.AbstractLeetcodeQuestion;
import com.flaming.model.TreeNode;

//Given the root of a binary tree, return the leftmost value in the last row of
//the tree.
//
//
// Example 1:
//
//
//Input: root = [2,1,3]
//Output: 1
//
//
// Example 2:
//
//
//Input: root = [1,2,3,4,null,5,6,null,null,7]
//Output: 7
//
//
//
// Constraints:
//
//
// The number of nodes in the tree is in the range [1, 10⁴].
// -2³¹ <= Node.val <= 2³¹ - 1
//
// Related Topics 树 深度优先搜索 广度优先搜索 二叉树 👍 237 👎 0
public class Question513 extends AbstractLeetcodeQuestion {

    private int maxHeight = 0;
    private int result = -1;

    public int findBottomLeftValue(TreeNode root) {
        dfs(root, 1);
        return this.result;
    }

    public void dfs(TreeNode root, int height) {
        if (null == root) {
            return;
        }

        if (height > this.maxHeight) {
            this.maxHeight = height;
            this.result = root.val;
        }
        dfs(root.left, height + 1);
        dfs(root.right, height + 1);
    }

    @Override
    protected void test() {
        TreeNode root = new TreeNode(1,
                new TreeNode(2, new TreeNode(4, null, new TreeNode(7)), null),
                new TreeNode(3, new TreeNode(5), new TreeNode(6)));
        System.out.println(findBottomLeftValue(root));
    }
}
