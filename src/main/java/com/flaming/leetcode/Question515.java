package com.flaming.leetcode;

import com.flaming.AbstractLeetcodeQuestion;
import com.flaming.model.TreeNode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

//Given the root of a binary tree, return an array of the largest value in each
//row of the tree (0-indexed).
//
//
// Example 1:
//
//
//Input: root = [1,3,2,5,3,null,9]
//Output: [1,3,9]
//
//
// Example 2:
//
//
//Input: root = [1,2,3]
//Output: [1,3]
//
//
//
// Constraints:
//
//
// The number of nodes in the tree will be in the range [0, 10⁴].
// -2³¹ <= Node.val <= 2³¹ - 1
//
// Related Topics 树 深度优先搜索 广度优先搜索 二叉树 👍 166 👎 0
public class Question515 extends AbstractLeetcodeQuestion {

    public List<Integer> largestValues(TreeNode root) {
        if (null == root) {
            return new ArrayList<>();
        }

        List<Integer> results = new ArrayList<>();
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);
        while (!queue.isEmpty()) {
            int size = queue.size();
            Integer maxValue = null;
            for (int i = 0; i < size; i++) {
                TreeNode curNode = queue.poll();
                if (null == curNode) {
                    continue;
                }
                if (null == maxValue || curNode.val > maxValue) {
                    maxValue = curNode.val;
                }
                queue.offer(curNode.left);
                queue.offer(curNode.right);
            }
            if (null != maxValue) {
                results.add(maxValue);
            }
        }
        return results;
    }

    @Override
    protected void test() {
        TreeNode root = new TreeNode(1,
                new TreeNode(3, new TreeNode(5), new TreeNode(3)),
                new TreeNode(2, null, new TreeNode(9)));
        System.out.println(largestValues(root));
    }

}
