package com.flaming.leetcode;

import com.flaming.AbstractLeetcodeQuestion;
import com.flaming.model.TreeNode;

//Given a binary search tree with non-negative values, find the minimum absolute
// difference between values of any two nodes.
//
// Example:
//
//
//Input:
//
//   1
//    \
//     3
//    /
//   2
//
//Output:
//1
//
//Explanation:
//The minimum absolute difference is 1, which is the difference between 2 and 1
//(or between 2 and 3).
//
//
//
//
// Note:
//
//
// There are at least two nodes in this BST.
// This question is the same as 783: https://leetcode.com/problems/minimum-dista
//nce-between-bst-nodes/
//
// Related Topics 树
// 👍 230 👎 0
public class Question530 extends AbstractLeetcodeQuestion {

    private int result = Integer.MAX_VALUE;
    private Integer lastValue = null;

    public int getMinimumDifference(TreeNode root) {
        if (null == root) {
            return 0;
        }

        TreeNode curNode = root;
        TreeNode preNode = null;
        while (null != curNode) {
            if (result == 0) {
                break;
            }
            preNode = curNode.left;
            if (null == preNode) {
                visit(curNode);
                curNode = curNode.right;
                continue;
            }
            while (null != preNode.right && curNode != preNode.right) {
                preNode = preNode.right;
            }
            if (null == preNode.right) {
                preNode.right = curNode;
                curNode = curNode.left;
                continue;
            }
            visit(curNode);
            preNode.right = null;
            curNode = curNode.right;
        }

        return result;
    }

    private void visit(TreeNode root) {
        if (null == root) {
            return;
        }

        if (null != lastValue && root.val - lastValue < result) {
            result = root.val - lastValue;
        }

        lastValue = root.val;
    }

    @Override
    protected void beforeTest() {
        super.beforeTest();
        this.result = Integer.MAX_VALUE;
        this.lastValue = null;
    }

    @Override
    protected void test() {
        TreeNode root = new TreeNode(1, null, new TreeNode(3, new TreeNode(2), null));
        System.out.println(getMinimumDifference(root));
        this.beforeTest();
        root = new TreeNode(1, null, new TreeNode(5, new TreeNode(3), null));
        System.out.println(getMinimumDifference(root));
    }
}
