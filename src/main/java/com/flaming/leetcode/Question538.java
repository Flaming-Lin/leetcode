package com.flaming.leetcode;

import com.flaming.AbstractLeetcodeQuestion;
import com.flaming.model.TreeNode;

import java.util.Stack;

//Given the root of a Binary Search Tree (BST), convert it to a Greater Tree
//such that every key of the original BST is changed to the original key plus the
//sum of all keys greater than the original key in BST.
//
// As a reminder, a binary search tree is a tree that satisfies these
//constraints:
//
//
// The left subtree of a node contains only nodes with keys less than the
//node's key.
// The right subtree of a node contains only nodes with keys greater than the
//node's key.
// Both the left and right subtrees must also be binary search trees.
//
//
//
// Example 1:
//
//
//Input: root = [4,1,6,0,2,5,7,null,null,null,3,null,null,null,8]
//Output: [30,36,21,36,35,26,15,null,null,null,33,null,null,null,8]
//
//
// Example 2:
//
//
//Input: root = [0,null,1]
//Output: [1,null,1]
//
//
//
// Constraints:
//
//
// The number of nodes in the tree is in the range [0, 10⁴].
// -10⁴ <= Node.val <= 10⁴
// All the values in the tree are unique.
// root is guaranteed to be a valid binary search tree.
//
//
//
// Note: This question is the same as 1038: https://leetcode.com/problems/
//binary-search-tree-to-greater-sum-tree/
// Related Topics 树 深度优先搜索 二叉搜索树 二叉树 👍 642 👎 0
public class Question538 extends AbstractLeetcodeQuestion {

    public TreeNode convertBST(TreeNode root) {
        if (null == root) {
            return null;
        }

        TreeNode originalRoot = root;
        int currentSum = 0;
        TreeNode curNode = root;
        TreeNode preNode = null;
        while (null != curNode) {
            preNode = curNode.right;
            if (null == preNode) {
                currentSum += curNode.val;
                curNode.val = currentSum;
                curNode = curNode.left;
                continue;
            }
            while (null != preNode.left && curNode != preNode.left) {
                preNode = preNode.left;
            }
            if (curNode != preNode.left) {
                preNode.left = curNode;
                curNode = curNode.right;
                continue;
            }
            currentSum += curNode.val;
            curNode.val = currentSum;
            preNode.left = null;
            curNode = curNode.left;
        }
        return originalRoot;
    }

    @Override
    protected void test() {
        TreeNode root = new TreeNode(4,
                new TreeNode(1,
                        new TreeNode(0),
                        new TreeNode(2, null, new TreeNode(3))),
                new TreeNode(6,
                        new TreeNode(5),
                        new TreeNode(7, null, new TreeNode(8))));
        System.out.println(convertBST(root));
    }


}
