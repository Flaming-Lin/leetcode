package com.flaming.leetcode;

import com.flaming.AbstractLeetcodeQuestion;

import java.util.ArrayList;
import java.util.List;

//Given an m x n matrix, return all elements of the matrix in spiral order.
//
//
// Example 1:
//
//
//Input: matrix = [[1,2,3],[4,5,6],[7,8,9]]
//Output: [1,2,3,6,9,8,7,4,5]
//
//
// Example 2:
//
//
//Input: matrix = [[1,2,3,4],[5,6,7,8],[9,10,11,12]]
//Output: [1,2,3,4,8,12,11,10,9,5,6,7]
//
//
//
// Constraints:
//
//
// m == matrix.length
// n == matrix[i].length
// 1 <= m, n <= 10
// -100 <= matrix[i][j] <= 100
//
// Related Topics 数组
// 👍 620 👎 0
public class Question54 extends AbstractLeetcodeQuestion {

    public List<Integer> spiralOrder(int[][] matrix) {
        List<Integer> results = new ArrayList<>();

        int top = 0;
        int bottom = matrix.length - 1;
        int left = 0;
        int right = matrix[0].length - 1;

        while (top <= bottom && left <= right) {
            for (int topEdge = left; topEdge <= right; topEdge++) {
                results.add(matrix[top][topEdge]);
            }
            top++;
            for (int rightEdge = top; rightEdge <= bottom; rightEdge++) {
                results.add(matrix[rightEdge][right]);
            }
            right--;
            if (top > bottom || left > right) {
                break;
            }
            for (int bottomEdge = right; bottomEdge >= left; bottomEdge--) {
                results.add(matrix[bottom][bottomEdge]);
            }
            bottom--;
            for (int leftEdge = bottom; leftEdge >= top; leftEdge--) {
                results.add(matrix[leftEdge][left]);
            }
            left++;
        }

        return results;
    }

    @Override
    protected void test() {
        int[][] matrix = new int[][]{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        System.out.println(spiralOrder(matrix));
        matrix = new int[][]{{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12}};
        System.out.println(spiralOrder(matrix));
    }
}
