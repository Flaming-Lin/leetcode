package com.flaming.leetcode;

import com.flaming.AbstractLeetcodeQuestion;
import com.flaming.model.TreeNode;

//Given the root of a binary tree, return the length of the diameter of the
//tree.
//
// The diameter of a binary tree is the length of the longest path between any
//two nodes in a tree. This path may or may not pass through the root.
//
// The length of a path between two nodes is represented by the number of edges
//between them.
//
//
// Example 1:
//
//
//Input: root = [1,2,3,4,5]
//Output: 3
//Explanation: 3 is the length of the path [4,2,1,3] or [5,2,1,3].
//
//
// Example 2:
//
//
//Input: root = [1,2]
//Output: 1
//
//
//
// Constraints:
//
//
// The number of nodes in the tree is in the range [1, 10⁴].
// -100 <= Node.val <= 100
//
// Related Topics 树 深度优先搜索 二叉树 👍 892 👎 0


//leetcode submit region begin(Prohibit modification and deletion)
public class Question543 extends AbstractLeetcodeQuestion {

    int maxDiameter = 0;

    public int diameterOfBinaryTree(TreeNode root) {
        getMaxLength(root);
        return maxDiameter;
    }

    public int getMaxLength(TreeNode root) {
        if (null == root) {
            return 0;
        }

        int leftLength = getMaxLength(root.left);
        int rightLength = getMaxLength(root.right);
        if (leftLength + rightLength > maxDiameter) {
            maxDiameter = leftLength + rightLength;
        }
        return 1 + Math.max(leftLength, rightLength);
    }

    @Override
    protected void test() {
        TreeNode root = new TreeNode(1,
                new TreeNode(2, new TreeNode(4), new TreeNode(5)),
                new TreeNode(3));
        System.out.println(diameterOfBinaryTree(root));
    }
}
