package com.flaming.leetcode;

import com.flaming.AbstractLeetcodeQuestion;

//A Binary Matrix is a matrix in which all the elements are either 0 or 1.
//
// Given quadTree1 and quadTree2. quadTree1 represents a n * n binary matrix
//and quadTree2 represents another n * n binary matrix.
//
// Return a Quad-Tree representing the n * n binary matrix which is the result
//of logical bitwise OR of the two binary matrixes represented by quadTree1 and
//quadTree2.
//
// Notice that you can assign the value of a node to True or False when isLeaf
//is False, and both are accepted in the answer.
//
// A Quad-Tree is a tree data structure in which each internal node has exactly
//four children. Besides, each node has two attributes:
//
//
// val: True if the node represents a grid of 1's or False if the node
//represents a grid of 0's.
// isLeaf: True if the node is leaf node on the tree or False if the node has
//the four children.
public class Question558 extends AbstractLeetcodeQuestion {

    public Node intersect(Node quadTree1, Node quadTree2) {
        if (null == quadTree1 || null == quadTree2) {
            return null;
        }

        if (quadTree1.isLeaf && quadTree2.isLeaf) {
            return new Node(quadTree1.val || quadTree2.val, true, null, null, null, null);
        }
        if (quadTree1.isLeaf) {
            quadTree1 = new Node(quadTree1.val, false,
                    new Node(quadTree1.val, true, null, null, null, null),
                    new Node(quadTree1.val, true, null, null, null, null),
                    new Node(quadTree1.val, true, null, null, null, null),
                    new Node(quadTree1.val, true, null, null, null, null)
            );
        }
        if (quadTree2.isLeaf) {
            quadTree2 = new Node(quadTree2.val, false,
                    new Node(quadTree2.val, true, null, null, null, null),
                    new Node(quadTree2.val, true, null, null, null, null),
                    new Node(quadTree2.val, true, null, null, null, null),
                    new Node(quadTree2.val, true, null, null, null, null)
            );
        }
        Node topLeft = intersect(quadTree1.topLeft, quadTree2.topLeft);
        Node topRight = intersect(quadTree1.topRight, quadTree2.topRight);
        Node bottomLeft = intersect(quadTree1.bottomLeft, quadTree2.bottomLeft);
        Node bottomRight = intersect(quadTree1.bottomRight, quadTree2.bottomRight);
        if (topLeft.isLeaf && topRight.isLeaf && bottomLeft.isLeaf && bottomRight.isLeaf &&
                topLeft.val == topRight.val && topRight.val == bottomLeft.val && bottomLeft.val == bottomRight.val) {
            return new Node(topLeft.val, true, null, null, null, null);
        } else {
            return new Node(true, false, topLeft, topRight, bottomLeft, bottomRight);
        }
    }

    @Override
    protected void test() {
        Node quadTree1 = new Node(true, false,
                new Node(true, true, null, null, null, null),
                new Node(true, true, null, null, null, null),
                new Node(false, true, null, null, null, null),
                new Node(false, true, null, null, null, null)
        );
        Node quadTree2 = new Node(true, false,
                new Node(true, true, null, null, null, null),
                new Node(true, false,
                        new Node(false, true, null, null, null, null),
                        new Node(false, true, null, null, null, null),
                        new Node(true, true, null, null, null, null),
                        new Node(true, true, null, null, null, null)
                ),
                new Node(true, true, null, null, null, null),
                new Node(false, true, null, null, null, null)
        );
        System.out.println(intersect(quadTree1, quadTree2));

        quadTree1 = new Node(true, false,
                new Node(true, true, null, null, null, null),
                new Node(false, true, null, null, null, null),
                new Node(true, true, null, null, null, null),
                new Node(true, true, null, null, null, null)
        );
        quadTree2 = new Node(true, false,
                new Node(true, true, null, null, null, null),
                new Node(true, false,
                        new Node(true, true, null, null, null, null),
                        new Node(false, true, null, null, null, null),
                        new Node(false, true, null, null, null, null),
                        new Node(true, true, null, null, null, null)
                ),
                new Node(true, true, null, null, null, null),
                new Node(true, true, null, null, null, null)
        );
        System.out.println(intersect(quadTree1, quadTree2));
    }

    class Node {
        public boolean val;
        public boolean isLeaf;
        public Node topLeft;
        public Node topRight;
        public Node bottomLeft;
        public Node bottomRight;

        public Node() {}

        public Node(boolean _val,boolean _isLeaf,Node _topLeft,Node _topRight,Node _bottomLeft,Node _bottomRight) {
            val = _val;
            isLeaf = _isLeaf;
            topLeft = _topLeft;
            topRight = _topRight;
            bottomLeft = _bottomLeft;
            bottomRight = _bottomRight;
        }
    };

}
