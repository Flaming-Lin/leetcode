package com.flaming.leetcode;

import com.flaming.AbstractLeetcodeQuestion;

import java.util.Arrays;
import java.util.List;

//Given a n-ary tree, find its maximum depth.
//
// The maximum depth is the number of nodes along the longest path from the
//root node down to the farthest leaf node.
//
// Nary-Tree input serialization is represented in their level order traversal,
//each group of children is separated by the null value (See examples).
//
//
// Example 1:
//
//
//
//
//Input: root = [1,null,3,2,4,null,5,6]
//Output: 3
//
//
// Example 2:
//
//
//
//
//Input: root = [1,null,2,3,4,5,null,null,6,7,null,8,null,9,10,null,null,11,
//null,12,null,13,null,null,14]
//Output: 5
//
//
//
// Constraints:
//
//
// The total number of nodes is in the range [0, 10⁴].
// The depth of the n-ary tree is less than or equal to 1000.
//
// Related Topics 树 深度优先搜索 广度优先搜索 👍 248 👎 0


//leetcode submit region begin(Prohibit modification and deletion)
/*
// Definition for a Node.
class Node {
    public int val;
    public List<Node> children;

    public Node() {}

    public Node(int _val) {
        val = _val;
    }

    public Node(int _val, List<Node> _children) {
        val = _val;
        children = _children;
    }
};
*/
public class Question559 extends AbstractLeetcodeQuestion {

    public int maxDepth(Node root) {
        if (null == root) {
            return 0;
        }
        if (null == root.children || root.children.isEmpty()) {
            return 1;
        }
        int maxDepth = 0;
        for (Node node : root.children) {
            maxDepth = Math.max(maxDepth(node) + 1, maxDepth);
        }
        return maxDepth;
    }

    @Override
    protected void test() {
        Node root = new Node(1, Arrays.asList(
                new Node(3, Arrays.asList(
                        new Node(5),
                        new Node(6))),
                new Node(2),
                new Node(4)));
        System.out.println(maxDepth(root));
    }

    class Node {
        public int val;
        public List<Node> children;

        public Node() {}

        public Node(int _val) {
            val = _val;
        }

        public Node(int _val, List<Node> _children) {
            val = _val;
            children = _children;
        }
    };

}
