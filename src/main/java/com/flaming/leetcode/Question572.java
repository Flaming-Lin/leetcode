package com.flaming.leetcode;

import com.flaming.AbstractLeetcodeQuestion;
import com.flaming.model.TreeNode;

//Given the roots of two binary trees root and subRoot, return true if there is
//a subtree of root with the same structure and node values of subRoot and false
//otherwise.
//
// A subtree of a binary tree tree is a tree that consists of a node in tree
//and all of this node's descendants. The tree tree could also be considered as a
//subtree of itself.
//
//
// Example 1:
//
//
//Input: root = [3,4,5,1,2], subRoot = [4,1,2]
//Output: true
//
//
// Example 2:
//
//
//Input: root = [3,4,5,1,2,null,null,null,null,0], subRoot = [4,1,2]
//Output: false
//
//
//
// Constraints:
//
//
// The number of nodes in the root tree is in the range [1, 2000].
// The number of nodes in the subRoot tree is in the range [1, 1000].
// -10⁴ <= root.val <= 10⁴
// -10⁴ <= subRoot.val <= 10⁴
//
// Related Topics 树 深度优先搜索 二叉树 字符串匹配 哈希函数 👍 633 👎 0
public class Question572 extends AbstractLeetcodeQuestion {

    public boolean isSubtree(TreeNode root, TreeNode subRoot) {
        if (null == root && null == subRoot) {
            return true;
        }
        if (null == root || null == subRoot) {
            return false;
        }


        if (root.val == subRoot.val) {
            boolean isSame = isSame(root.left, subRoot.left) && isSame(root.right, subRoot.right);
            if (isSame) {
                return true;
            }
        }

        return isSubtree(root.left, subRoot) || isSubtree(root.right, subRoot);
    }

    private boolean isSame(TreeNode root1, TreeNode root2) {
        if (null == root1 && null == root2) {
            return true;
        }
        if (null == root1 || null == root2) {
            return false;
        }

        return root1.val == root2.val && isSame(root1.left, root2.left) && isSame(root1.right, root2.right);
    }

    @Override
    protected void test() {
        TreeNode root = new TreeNode(3,
                new TreeNode(4, new TreeNode(1), new TreeNode(2)),
                new TreeNode(5));
        TreeNode subRoot = new TreeNode(4, new TreeNode(1), new TreeNode(2));
        System.out.println(isSubtree(root, subRoot));
    }
}
