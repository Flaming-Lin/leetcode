package com.flaming.leetcode;

import com.flaming.AbstractLeetcodeQuestion;

import java.util.*;

//Given the root of an n-ary tree, return the preorder traversal of its nodes'
//values.
//
// Nary-Tree input serialization is represented in their level order traversal.
//Each group of children is separated by the null value (See examples)
//
//
// Example 1:
//
//
//
//
//Input: root = [1,null,3,2,4,null,5,6]
//Output: [1,3,5,6,2,4]
//
//
// Example 2:
//
//
//
//
//Input: root = [1,null,2,3,4,5,null,null,6,7,null,8,null,9,10,null,null,11,
//null,12,null,13,null,null,14]
//Output: [1,2,3,6,7,11,14,4,8,12,5,9,13,10]
//
//
//
// Constraints:
//
//
// The number of nodes in the tree is in the range [0, 10⁴].
// 0 <= Node.val <= 10⁴
// The height of the n-ary tree is less than or equal to 1000.
//
//
//
// Follow up: Recursive solution is trivial, could you do it iteratively?
// Related Topics 栈 树 深度优先搜索 👍 202 👎 0
public class Question589 extends AbstractLeetcodeQuestion {

    public List<Integer> preorder(Node root) {
        if (null == root) {
            return new ArrayList<>();
        }

        List<Integer> results = new ArrayList<>();
        Stack<Node> stack = new Stack<>();
        stack.push(root);
        while (!stack.isEmpty()) {
            root = stack.pop();
            if (null == root) {
                continue;
            }
            results.add(root.val);
            if (null == root.children) {
                continue;
            }
            for (int i = root.children.size() - 1; i >= 0; i--) {
                stack.push(root.children.get(i));
            }
        }
        return results;
    }

    @Override
    protected void test() {
        Node root = new Node(1, Arrays.asList(
                new Node(3, Arrays.asList(new Node(5), new Node(6))),
                new Node(2),
                new Node(4)));
        System.out.println(preorder(root));
    }

    class Node {
        public int val;
        public List<Node> children;

        public Node() {
        }

        public Node(int _val) {
            val = _val;
        }

        public Node(int _val, List<Node> _children) {
            val = _val;
            children = _children;
        }
    }

    ;

}
