package com.flaming.leetcode;

import com.flaming.AbstractLeetcodeQuestion;
import com.flaming.model.TreeNode;

import java.util.LinkedList;
import java.util.Queue;

//Given the root of a binary tree and two integers val and depth, add a row of
//nodes with value val at the given depth depth.
//
// Note that the root node is at depth 1.
//
// The adding rule is:
//
//
// Given the integer depth, for each not null tree node cur at the depth depth -
// 1, create two tree nodes with value val as cur's left subtree root and right
//subtree root.
// cur's original left subtree should be the left subtree of the new left
//subtree root.
// cur's original right subtree should be the right subtree of the new right
//subtree root.
// If depth == 1 that means there is no depth depth - 1 at all, then create a
//tree node with value val as the new root of the whole original tree, and the
//original tree is the new root's left subtree.
//
//
//
// Example 1:
//
//
//Input: root = [4,2,6,3,1,5], val = 1, depth = 2
//Output: [4,1,1,2,null,null,6,3,1,5]
//
//
// Example 2:
//
//
//Input: root = [4,2,null,3,1], val = 1, depth = 3
//Output: [4,2,null,1,1,3,null,null,1]
//
//
//
// Constraints:
//
//
// The number of nodes in the tree is in the range [1, 10⁴].
// The depth of the tree is in the range [1, 10⁴].
// -100 <= Node.val <= 100
// -10⁵ <= val <= 10⁵
// 1 <= depth <= the depth of tree + 1
//
// Related Topics 树 深度优先搜索 广度优先搜索 二叉树 👍 107 👎 0
public class Question623 extends AbstractLeetcodeQuestion {

    public TreeNode addOneRow(TreeNode root, int val, int depth) {
        if (null == root) {
            return null;
        }

        if (depth == 1) {
            return new TreeNode(val, root, null);
        }

        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);
        int curDepth = 1;
        while (!queue.isEmpty()) {
            int size = queue.size();
            for (int i = 0; i < size; i++) {
                TreeNode curNode = queue.poll();
                if (curDepth == depth - 1) {
                    TreeNode tmpNode = curNode.left;
                    curNode.left = new TreeNode(val, tmpNode, null);
                    tmpNode = curNode.right;
                    curNode.right = new TreeNode(val, null, tmpNode);
                }
                if (null != curNode.left) {
                    queue.offer(curNode.left);
                }
                if (null != curNode.right) {
                    queue.offer(curNode.right);
                }
            }
            curDepth++;
            if (curDepth >= depth) {
                break;
            }
        }
        return root;
    }

    @Override
    protected void test() {
        TreeNode root = new TreeNode(4,
                new TreeNode(2, new TreeNode(3, new TreeNode(5), null), new TreeNode(1)),
                new TreeNode(6));
        System.out.println(addOneRow(root, 1, 2));
    }

}
