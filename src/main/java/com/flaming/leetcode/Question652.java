package com.flaming.leetcode;

import com.flaming.AbstractLeetcodeQuestion;
import com.flaming.model.TreeNode;

import java.util.*;

//Given the root of a binary tree, return all duplicate subtrees.
//
// For each kind of duplicate subtrees, you only need to return the root node
//of any one of them.
//
// Two trees are duplicate if they have the same structure with the same node
//values.
//
//
// Example 1:
//
//
//Input: root = [1,2,3,4,null,2,4,null,null,4]
//Output: [[2,4],[4]]
//
//
// Example 2:
//
//
//Input: root = [2,1,1]
//Output: [[1]]
//
//
// Example 3:
//
//
//Input: root = [2,2,2,3,null,3,null]
//Output: [[2,3],[3]]
//
//
//
// Constraints:
//
//
// The number of the nodes in the tree will be in the range [1, 10^4]
// -200 <= Node.val <= 200
//
// Related Topics 树 深度优先搜索 广度优先搜索 二叉树 👍 365 👎 0
public class Question652 extends AbstractLeetcodeQuestion {

    private Map<String, Integer> uniqueCodeMap = new HashMap<>();
    private List<TreeNode> results = new ArrayList<>();

    public List<TreeNode> findDuplicateSubtrees(TreeNode root) {
        if (null == root) {
            return new ArrayList<>();
        }
        getUniqueCode(root);
        return this.results;
    }

    public String getUniqueCode(TreeNode root) {
        if (null == root) {
            return "#";
        }

        String uniqueCode = "(" + root.val + "," + getUniqueCode(root.left) + "," + getUniqueCode(root.right) + ")";
        this.uniqueCodeMap.putIfAbsent(uniqueCode, 0);
        this.uniqueCodeMap.put(uniqueCode, uniqueCodeMap.get(uniqueCode) + 1);
        if (uniqueCodeMap.get(uniqueCode) == 2) {
            this.results.add(root);
        }
        return uniqueCode;
    }



    @Override
    protected void test() {
        // [1,2,3,4,null,2,4,null,null,4]
        TreeNode root = new TreeNode(1,
                new TreeNode(2, new TreeNode(4), null),
                new TreeNode(3, new TreeNode(2, new TreeNode(4), null), new TreeNode(4))
        );
        System.out.println(findDuplicateSubtrees(root));
    }
}
