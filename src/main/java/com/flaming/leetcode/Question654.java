package com.flaming.leetcode;

import com.flaming.AbstractLeetcodeQuestion;
import com.flaming.model.TreeNode;

//
//Given an integer array with no duplicates. A maximum tree building on this arr
//ay is defined as follow:
//
// The root is the maximum number in the array.
// The left subtree is the maximum tree constructed from left part subarray divi
//ded by the maximum number.
// The right subtree is the maximum tree constructed from right part subarray di
//vided by the maximum number.
//
//
//
//
//Construct the maximum tree by the given array and output the root node of this
// tree.
//
//
// Example 1:
//
//Input: [3,2,1,6,0,5]
//Output: return the tree root node representing the following tree:
//
//      6
//    /   \
//   3     5
//    \    /
//     2  0
//       \
//        1
//
//
//
// Note:
//
// The size of the given array will be in the range [1,1000].
//
// Related Topics 树
// 👍 208 👎 0
public class Question654 extends AbstractLeetcodeQuestion {

    public TreeNode constructMaximumBinaryTree(int[] nums) {
        return constructMaximumBinaryTree(nums, 0, nums.length - 1);
    }

    private TreeNode constructMaximumBinaryTree(int[] nums, int left, int right) {
        if (left > right) {
            return null;
        }
        int max = Integer.MIN_VALUE;
        int maxIndex = -1;
        for (int i = left; i <= right; i++) {
            if (nums[i] > max) {
                maxIndex = i;
                max = nums[i];
            }
        }
        return new TreeNode(max,
                constructMaximumBinaryTree(nums, left, maxIndex - 1),
                constructMaximumBinaryTree(nums, maxIndex + 1, right));
    }

    @Override
    protected void test() {
        int[] nums = {3,2,1,6,0,5};
        System.out.println(constructMaximumBinaryTree(nums));
    }
}
