package com.flaming.leetcode;

import com.flaming.AbstractLeetcodeQuestion;

import java.util.Arrays;

//There is an undirected connected tree with n nodes labeled from 0 to n - 1
//and n - 1 edges.
//
// You are given the integer n and the array edges where edges[i] = [ai, bi]
//indicates that there is an edge between nodes ai and bi in the tree.
//
// Return an array answer of length n where answer[i] is the sum of the
//distances between the iᵗʰ node in the tree and all other nodes.
//
//
// Example 1:
//
//
//Input: n = 6, edges = [[0,1],[0,2],[2,3],[2,4],[2,5]]
//Output: [8,12,6,10,10,10]
//Explanation: The tree is shown above.
//We can see that dist(0,1) + dist(0,2) + dist(0,3) + dist(0,4) + dist(0,5)
//equals 1 + 1 + 2 + 2 + 2 = 8.
//Hence, answer[0] = 8, and so on.
//
//
// Example 2:
//
//
//Input: n = 1, edges = []
//Output: [0]
//
//
// Example 3:
//
//
//Input: n = 2, edges = [[1,0]]
//Output: [1,1]
//
//
//
// Constraints:
//
//
// 1 <= n <= 3 * 10⁴
// edges.length == n - 1
// edges[i].length == 2
// 0 <= ai, bi < n
// ai != bi
// The given input represents a valid tree.
//
// Related Topics 树 深度优先搜索 图 动态规划 👍 325 👎 0
public abstract class Question834 extends AbstractLeetcodeQuestion {

    public abstract int[] sumOfDistancesInTree(int n, int[][] edges);

    @Override
    protected void test() {
        int[][] edges = new int[][]{
                new int[]{0, 1},
                new int[]{0, 2},
                new int[]{2, 3},
                new int[]{2, 4},
                new int[]{2, 5}
        };
        System.out.println(Arrays.toString(sumOfDistancesInTree(6, edges)));

        beforeTest();
        edges = new int[][]{
                new int[]{0, 2},
                new int[]{1, 2}
        };
        System.out.println(Arrays.toString(sumOfDistancesInTree(3, edges)));

        beforeTest();
        edges = new int[][]{
                new int[]{3, 1},
                new int[]{6, 1},
                new int[]{0, 5},
                new int[]{2, 5},
                new int[]{4, 1},
                new int[]{2, 1},
        };
        // Expected:[18,9,10,14,14,13,14]
        System.out.println(Arrays.toString(sumOfDistancesInTree(7, edges)));
    }

}
