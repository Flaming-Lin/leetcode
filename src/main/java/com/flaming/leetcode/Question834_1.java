package com.flaming.leetcode;

import java.util.LinkedList;
import java.util.Queue;

public class Question834_1 extends Question834 {

    class Point {
        int x;
        int y;

        public Point(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }

    /**
     * 自己想的一种思路，距离转变成二维矩阵，从距离1开始，每次循环 + 1，直到把所有两个节点之间的距离值填满
     * <p>
     * 但是超时！！！跑不通.....
     */
    @Override
    public int[] sumOfDistancesInTree(int n, int[][] edges) {

        int[][] distance = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                distance[i][j] = (i == j) ? 0 : -1;
            }
        }
        Queue<Point> queue = new LinkedList<>();
        for (int i = 0; i < edges.length; i++) {
            int x = edges[i][0];
            int y = edges[i][1];
            queue.offer(new Point(x, y));
            queue.offer(new Point(y, x));
            distance[x][y] = distance[y][x] = 1;
        }

        while (!queue.isEmpty()) {
            int size = queue.size();
            for (int i = 0; i < size; i++) {
                Point point = queue.poll();
                int start = point.x;
                int end = point.y;
                for (int j = 0; j < n; j++) {
                    if (distance[end][j] <= 0) {
                        continue;
                    }

                    int totalDistance = distance[start][end] + distance[end][j];
                    if (distance[start][j] < 0 || totalDistance < distance[start][j]) {
                        distance[start][j] = distance[j][start] = totalDistance;
                        queue.offer(new Point(start, j));
                        queue.offer(new Point(j, start));
                    }
                }
            }
        }

        int[] results = new int[n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                results[i] += distance[i][j];
            }
        }
        return results;
    }

}
