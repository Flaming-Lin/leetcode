package com.flaming.leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Question834_2 extends Question834 {

    private List<List<Integer>> neighborMap = new ArrayList<>();
    private int[] distanceSum;
    private int[] nodeSum;

    /**
     * 此种解法的核心思想是对于某个节点，分「其子树」与「其他节点」两种情况来解
     * 两部分都是通过 "数路径" 的办法，找到路径与节点个数的关系
     * 前者通过后序遍历自下而上逐渐推导出正确值，而后者通过自上而下，由根节点的正确值逐渐计算完成
     */
    @Override
    public int[] sumOfDistancesInTree(int n, int[][] edges) {
        for (int i = 0; i < n; i++) {
            this.neighborMap.add(new ArrayList<>());
        }
        for (int i = 0; i < edges.length; i++) {
            int start = edges[i][0];
            int end = edges[i][1];
            this.neighborMap.get(start).add(end);
            this.neighborMap.get(end).add(start);
        }
        this.distanceSum = new int[n];
        this.nodeSum = new int[n];
        Arrays.fill(this.nodeSum, 1);

        postOrder(0, -1);
        preOrder(0, -1);

        return this.distanceSum;
    }

    private void postOrder(int root, int parent) {
        List<Integer> neighbors = this.neighborMap.get(root);
        for (int neighbor : neighbors) {
            if (neighbor == parent) {
                continue;
            }

            postOrder(neighbor, root);
            this.nodeSum[root] += this.nodeSum[neighbor];
            this.distanceSum[root] += this.distanceSum[neighbor] + this.nodeSum[neighbor];
        }
    }

    private void preOrder(int root, int parent) {
        List<Integer> neighbors = this.neighborMap.get(root);
        for (int neighbor : neighbors) {
            if (neighbor == parent) {
                continue;
            }

            this.distanceSum[neighbor] = this.distanceSum[root] - this.nodeSum[neighbor] + this.neighborMap.size() - this.nodeSum[neighbor];
            preOrder(neighbor, root);
        }
    }

    @Override
    protected void beforeTest() {
        this.neighborMap = new ArrayList<>();
        this.distanceSum = new int[0];
        this.nodeSum = new int[0];
    }
}
