package com.flaming.leetcode;

import com.flaming.AbstractLeetcodeQuestion;

//Determine whether an integer is a palindrome. An integer is a palindrome when
//it reads the same backward as forward.
//
// Follow up: Could you solve it without converting the integer to a string?
//
//
// Example 1:
//
//
//Input: x = 121
//Output: true
//
//
// Example 2:
//
//
//Input: x = -121
//Output: false
//Explanation: From left to right, it reads -121. From right to left, it becomes
// 121-. Therefore it is not a palindrome.
//
//
// Example 3:
//
//
//Input: x = 10
//Output: false
//Explanation: Reads 01 from right to left. Therefore it is not a palindrome.
//
//
// Example 4:
//
//
//Input: x = -101
//Output: false
//
//
//
// Constraints:
//
//
// -231 <= x <= 231 - 1
//
// Related Topics 数学
// 👍 1291 👎 0
public class Question9 extends AbstractLeetcodeQuestion {

    public boolean isPalindrome(int x) {
        if (x < 0 || (x >= 10 && x % 10 == 0)) {
            return false;
        }

        int reverse = 0;
        while (reverse < x) {
            int pop = x % 10;
            x = x / 10;
            reverse = reverse * 10 + pop;
        }
        return reverse == x || reverse / 10 == x;
    }

    @Override
    protected void test() {
        System.out.println(isPalindrome(121));
        System.out.println(isPalindrome(1215));
    }

}
