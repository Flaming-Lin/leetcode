package com.flaming.leetcode;

import com.flaming.AbstractLeetcodeQuestion;

import java.util.Arrays;

//Given an array of integers nums, half of the integers in nums are odd, and the
// other half are even.
//
// Sort the array so that whenever nums[i] is odd, i is odd, and whenever nums[i
//] is even, i is even.
//
// Return any answer array that satisfies this condition.
//
//
// Example 1:
//
//
//Input: nums = [4,2,5,7]
//Output: [4,5,2,7]
//Explanation: [4,7,2,5], [2,5,4,7], [2,7,4,5] would also have been accepted.
//
//
// Example 2:
//
//
//Input: nums = [2,3]
//Output: [2,3]
//
//
//
// Constraints:
//
//
// 2 <= nums.length <= 2 * 104
// nums.length is even.
// Half of the integers in nums are even.
// 0 <= nums[i] <= 1000
//
// Related Topics 排序 数组
// 👍 195 👎 0
public class Question922 extends AbstractLeetcodeQuestion {

    public int[] sortArrayByParityII(int[] nums) {
        for (int even = 0, odd = 1; even < nums.length && odd < nums.length; even += 2) {
            if (nums[even] % 2 == 0) {
                continue;
            }

            for (; odd < nums.length; odd += 2) {
                if (nums[odd] % 2 == 0) {
                    break;
                }
            }

            int tmp = nums[odd];
            nums[odd] = nums[even];
            nums[even] = tmp;
            odd += 2;
        }
        return nums;
    }

    @Override
    protected void test() {
        int[] nums = new int[] {4, 2, 5, 7};
        System.out.println(Arrays.toString(sortArrayByParityII(nums)));
    }

}
