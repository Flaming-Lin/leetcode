package com.flaming.leetcode;

import com.flaming.AbstractLeetcodeQuestion;
import com.flaming.model.TreeNode;

import java.util.List;

//Given an integer n, generate all structurally unique BST's (binary search tree
//s) that store values 1 ... n.
//
// Example:
//
//
//Input: 3
//Output:
//[
//  [1,null,3,2],
//  [3,2,null,1],
//  [3,1,null,null,2],
//  [2,1,3],
//  [1,null,2,null,3]
//]
//Explanation:
//The above output corresponds to the 5 unique BST's shown below:
//
//   1         3     3      2      1
//    \       /     /      / \      \
//     3     2     1      1   3      2
//    /     /       \                 \
//   2     1         2                 3
//
//
//
// Constraints:
//
//
// 0 <= n <= 8
//
// Related Topics 树 动态规划
// 👍 687 👎 0
public abstract class Question95 extends AbstractLeetcodeQuestion {

    protected abstract List<TreeNode> generateTrees(int n);

    @Override
    protected void test() {
        for (TreeNode treeNode : generateTrees(3)) {
            System.out.println(treeNode);
        }
    }
}
