package com.flaming.leetcode;

import com.flaming.AbstractLeetcodeQuestion;
import com.flaming.model.TreeNode;

import java.util.LinkedList;
import java.util.Queue;

//Given the root of a binary tree, determine if it is a complete binary tree.
//
// In a complete binary tree, every level, except possibly the last, is complete
//ly filled, and all nodes in the last level are as far left as possible. It can h
//ave between 1 and 2h nodes inclusive at the last level h.
//
//
// Example 1:
//
//
//Input: root = [1,2,3,4,5,6]
//Output: true
//Explanation: Every level before the last is full (ie. levels with node-values
//{1} and {2, 3}), and all nodes in the last level ({4, 5, 6}) are as far left as
//possible.
//
//
// Example 2:
//
//
//Input: root = [1,2,3,4,5,null,7]
//Output: false
//Explanation: The node with value 7 isn't as far left as possible.
//
//
//
// Constraints:
//
//
// The number of nodes in the tree is in the range [1, 100].
// 1 <= Node.val <= 1000
//
// Related Topics 树
// 👍 107 👎 0
public class Question958 extends AbstractLeetcodeQuestion {

    public boolean isCompleteTree(TreeNode root) {
        if (null == root) {
            return false;
        }

        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);
        boolean reachEnd = false;
        while (!queue.isEmpty()) {
            TreeNode curNode = queue.poll();
            if (reachEnd && null != curNode) {
                return false;
            }
            if (null == curNode) {
                reachEnd = true;
                continue;
            }
            queue.offer(curNode.left);
            queue.offer(curNode.right);
        }
        return true;
    }

    @Override
    protected void test() {
        TreeNode root = new TreeNode(1, new TreeNode(2, new TreeNode(4), new TreeNode(5)), new TreeNode(3, new TreeNode(6), null));
        System.out.println(isCompleteTree(root));
    }
}
