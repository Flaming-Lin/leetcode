package com.flaming.leetcode;

import com.flaming.model.TreeNode;

import java.util.ArrayList;
import java.util.List;

public class Question95_1 extends Question95 {

    /**
     * 普通递归写法，因有重复计算导致性能较差。注意边界值 0 需要特殊处理
     */
    @Override
    public List<TreeNode> generateTrees(int n) {
        if (n <= 0) {
            return new ArrayList<>();
        }
        int[] arr = new int[n];
        for (int i = 1; i <= n; i++) {
            arr[i - 1] = i;
        }
        return generateTrees(arr, 0, n - 1);
    }

    private List<TreeNode> generateTrees(int[] arr, int left, int right) {
        List<TreeNode> resultList = new ArrayList<>();
        if (left > right) {
            resultList.add(null);
            return resultList;
        }

        for (int mid = left; mid <= right; mid++) {
            List<TreeNode> leftTrees = generateTrees(arr, left, mid - 1);
            List<TreeNode> rightTrees = generateTrees(arr, mid + 1, right);
            for (TreeNode leftTree : leftTrees) {
                for (TreeNode rightTree : rightTrees) {
                    TreeNode treeNode = new TreeNode(arr[mid]);
                    treeNode.left = leftTree;
                    treeNode.right = rightTree;
                    resultList.add(treeNode);
                }
            }
        }
        return resultList;
    }

}
