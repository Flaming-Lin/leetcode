package com.flaming.leetcode;

import com.flaming.model.TreeNode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Question95_2 extends Question95{

    /**
     * 初版动态规划实现。空间换时间记录了前序运行结果
     * - 注意最后的 getOffsetTree, 这里是因为 n 个数字的二叉树结果是固定数量，仅中间的数字不同。
     *   所以可以将后半段二叉树 [x+1, n] 试作 [1, n-x] 的结果集每个元素值偏移 x。
     */
    @Override
    protected List<TreeNode> generateTrees(int n) {
        if (n <= 0) {
            return new ArrayList<>();
        }

        Map<Integer, List<TreeNode>> dp = new HashMap<>(n);
        List<TreeNode> zeroNodes = new ArrayList<>();
        zeroNodes.add(null);
        dp.put(0, zeroNodes);
        for (int num = 1; num <= n; num++) {
            List<TreeNode> curNodes = new ArrayList<>();
            for (int x = 1; x <= num; x++) {
                List<TreeNode> leftNodes = dp.get(x - 1);
                List<TreeNode> rightNodes = dp.get(num - x);
                for (TreeNode leftNode : leftNodes) {
                    for (TreeNode rightNode : rightNodes) {
                        TreeNode treeNode = new TreeNode(x);
                        treeNode.left = leftNode;
                        treeNode.right = getOffsetTree(rightNode, x);
                        curNodes.add(treeNode);
                    }
                }
            }
            dp.put(num, curNodes);
        }
        return dp.get(n);
    }

    private TreeNode getOffsetTree(TreeNode root, int offset) {
        if (null == root) {
            return null;
        }
        return new TreeNode(root.val + offset, getOffsetTree(root.left, offset), getOffsetTree(root.right, offset));
    }
}
