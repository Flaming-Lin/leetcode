package com.flaming.leetcode;

import com.flaming.model.TreeNode;

import java.util.ArrayList;
import java.util.List;

public class Question95_3 extends Question95{

    /**
     * 第二种动态规划实现。
     * - 核心是通过分析其规律，发现对于已有的结果，新增一个数字x，仅可能出现在「根节点、右子树、右子树的右子树...」
     *   同时，被新增节点抢占位置的原有节点，一定是新增节点的左子树，因为新增的数字是最大的
     */
    @Override
    protected List<TreeNode> generateTrees(int n) {
        if (n <= 0) {
            return new ArrayList<>();
        }
        List<TreeNode> preNodes = new ArrayList<>();
        preNodes.add(null);
        for (int num = 1; num <= n; num++) {
            List<TreeNode> curNodes = new ArrayList<>();
            for (TreeNode preNode : preNodes) {
                // Insert Into Root.
                curNodes.add(new TreeNode(num, preNode, null));
                // Insert Into Right child.
                for (int level = 1; level <= num - 1; level++) {
                    TreeNode dummyNode = copyTree(preNode);
                    TreeNode curNode = dummyNode;
                    for (int rightIndex = 1; rightIndex <= level - 1 && null != curNode; rightIndex++) {
                        curNode = curNode.right;
                    }
                    if (null == curNode) {
                        break;
                    }
                    curNode.right = new TreeNode(num, curNode.right, null);
                    curNodes.add(dummyNode);
                }
            }
            preNodes = curNodes;
        }
        return preNodes;
    }

    private TreeNode copyTree(TreeNode root) {
        if (null == root) {
            return null;
        }
        return new TreeNode(root.val, copyTree(root.left), copyTree(root.right));
    }
}
