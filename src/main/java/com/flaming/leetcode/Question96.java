package com.flaming.leetcode;

import com.flaming.AbstractLeetcodeQuestion;

//Given n, how many structurally unique BST's (binary search trees) that store v
//alues 1 ... n?
//
// Example:
//
//
//Input: 3
//Output: 5
//Explanation:
//Given n = 3, there are a total of 5 unique BST's:
//
//   1         3     3      2      1
//    \       /     /      / \      \
//     3     2     1      1   3      2
//    /     /       \                 \
//   2     1         2                 3
//
//
//
// Constraints:
//
//
// 1 <= n <= 19
//
// Related Topics 树 动态规划
// 👍 876 👎 0
public abstract class Question96 extends AbstractLeetcodeQuestion {

    public abstract int numTrees(int n);

    @Override
    protected void test() {
        System.out.println(numTrees(3));
    }
}
