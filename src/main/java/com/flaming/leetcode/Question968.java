package com.flaming.leetcode;

import com.flaming.AbstractLeetcodeQuestion;
import com.flaming.model.TreeNode;

//You are given the root of a binary tree. We install cameras on the tree nodes
//where each camera at a node can monitor its parent, itself, and its immediate
//children.
//
// Return the minimum number of cameras needed to monitor all nodes of the tree.
//
//
//
// Example 1:
//
//
//Input: root = [0,0,null,0,0]
//Output: 1
//Explanation: One camera is enough to monitor all nodes if placed as shown.
//
//
// Example 2:
//
//
//Input: root = [0,0,null,0,null,0,null,null,0]
//Output: 2
//Explanation: At least two cameras are needed to monitor all nodes of the tree.
// The above image shows one of the valid configurations of camera placement.
//
//
//
// Constraints:
//
//
// The number of nodes in the tree is in the range [1, 1000].
// Node.val == 0
//
// Related Topics 树 深度优先搜索 动态规划 二叉树 👍 358 👎 0
public class Question968 extends AbstractLeetcodeQuestion {

    // TODO
    public int minCameraCover(TreeNode root) {
        return 0;
    }

    @Override
    protected void test() {

    }

}
