package com.flaming.leetcode;

public class Question96_1 extends Question96{

    /**
     * 简单版递归实现，不过耗时太长，可能 timeout
     */
    @Override
    public int numTrees(int n) {
        if (n <= 1) {
            return 1;
        }
        int total = 0;
        for (int x = 1; x <= n; x++) {
            total += numTrees(x - 1) * numTrees(n - x);
        }
        return total;
    }
}
