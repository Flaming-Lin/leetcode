package com.flaming.leetcode;

public class Question96_2 extends Question96{

    /**
     * 初版动态规划
     */
    @Override
    public int numTrees(int n) {
        if (n <= 1) {
            return 1;
        }
        int[] dp = new int[n + 1];
        dp[0] = 1;
        for (int num = 1; num <= n; num++) {
            int total = 0;
            for (int x = 1; x <= num; x++) {
                total += dp[x - 1] * dp[num - x];
            }
            dp[num] = total;
        }
        return dp[n];
    }
}
