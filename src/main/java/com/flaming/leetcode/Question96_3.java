package com.flaming.leetcode;

public class Question96_3 extends Question96{

    /**
     * 纯数学推导，这个还没看具体证明
     * 其实本体实质为计算卡塔兰数 *C_n*。卡塔兰数更便于计算的定义如下:
     *
     * ![C_0=1,\qquadC_{n+1}=\frac{2(2n+1)}{n+2}C_n ](./p__C_0_=_1,_qquad_C_{n+1}_=_frac{2_2n+1_}{n+2}C_n_.png)
     */
    @Override
    public int numTrees(int n) {
        long C = 1;
        for (int i = 0; i < n; ++i) {
            C = C * 2 * (2 * i + 1) / (i + 2);
        }
        return (int) C;
    }
}
