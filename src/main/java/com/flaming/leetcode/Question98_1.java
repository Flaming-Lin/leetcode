package com.flaming.leetcode;

import com.flaming.model.TreeNode;

public class Question98_1 extends Question98{

    @Override
    public boolean isValidBST(TreeNode root) {
        return isValidBST(root, null, null);
    }

    private boolean isValidBST(TreeNode root, Integer min, Integer max) {
        if (null == root) {
            return true;
        }
        return (null == min || root.val > min) &&
                (null == max || root.val < max) &&
                isValidBST(root.left, min, root.val) &&
                isValidBST(root.right, root.val, max);
    }
}
