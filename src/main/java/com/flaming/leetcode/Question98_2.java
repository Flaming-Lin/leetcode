package com.flaming.leetcode;

import com.flaming.model.TreeNode;

import java.util.Stack;

public class Question98_2 extends Question98{

    @Override
    public boolean isValidBST(TreeNode root) {
        if (null == root) {
            return true;
        }
        Stack<TreeNode> stack = new Stack<>();
        Integer pre = null;
        while (null != root || !stack.isEmpty()) {
            TreeNode curNode = root;
            while (null != curNode) {
                stack.push(curNode);
                curNode = curNode.left;
            }
            curNode = stack.pop();
            if (null == pre || curNode.val > pre) {
                pre = curNode.val;
            } else {
                return false;
            }
            root = curNode.right;
        }
        return true;
    }
}
