package com.flaming.leetcode;

import com.flaming.model.TreeNode;

import java.util.ArrayList;
import java.util.List;

public class Question99_1 extends Question99{

    /**
     * 主要依赖 BST 中序遍历的有序性，将树的问题转化为「递增数组两元素被交换」的问题
     * 而后，寻找 a[i] < a[i-1] 的错误元素坐标 x
     * - 如果找到两个，直接交换 x1 - 1 与 x2 的位置
     * - 如果找到一个，说明相邻，交换 x 与 x-1 的位置
     */
    @Override
    protected void recoverTree(TreeNode root) {
        if (null == root) {
            return;
        }
        List<TreeNode> inorderList = new ArrayList<>();
        inorderTraversal(root, inorderList);
        List<Integer> errorIndex = new ArrayList<>();
        TreeNode preNode = inorderList.get(0);
        for (int i = 1; i < inorderList.size(); i++) {
            if (inorderList.get(i).val < preNode.val) {
                errorIndex.add(i);
            }
            preNode = inorderList.get(i);
        }
        if (errorIndex.size() == 1) {
            Integer distinctIndex = errorIndex.get(0);
            swapTreeNode(inorderList.get(distinctIndex - 1), inorderList.get(distinctIndex));
        } else if (errorIndex.size() == 2) {
            Integer errorIndex1 = errorIndex.get(0);
            Integer errorIndex2 = errorIndex.get(1);
            swapTreeNode(inorderList.get(errorIndex1 - 1), inorderList.get(errorIndex2));
        }
    }

    private void swapTreeNode(TreeNode node1, TreeNode node2) {
        int tmp = node1.val;
        node1.val = node2.val;
        node2.val = tmp;
    }


    private void inorderTraversal(TreeNode root, List<TreeNode> results) {
        if (null == root) {
            return;
        }
        inorderTraversal(root.left, results);
        results.add(root);
        inorderTraversal(root.right, results);
    }

}
