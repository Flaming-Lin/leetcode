package com.flaming.leetcode;

import com.flaming.model.TreeNode;

import java.util.ArrayList;
import java.util.List;

public class Question99_2 extends Question99 {

    private TreeNode preNode;

    @Override
    protected void recoverTree(TreeNode root) {
        if (null == root) {
            return;
        }

        List<TreeNode> errorNodes = new ArrayList<>();
        inorderTraversal(root, errorNodes);
        if (errorNodes.size() == 2) {
            swapTreeNodes(errorNodes.get(0), errorNodes.get(1));
        } else if (errorNodes.size() == 3){
            swapTreeNodes(errorNodes.get(0), errorNodes.get(2));
        }
    }

    private void swapTreeNodes(TreeNode node1, TreeNode node2) {
        int tmp = node1.val;
        node1.val = node2.val;
        node2.val = tmp;
    }

    private void inorderTraversal(TreeNode root, List<TreeNode> errorNodes) {
        if (null == root) {
            return;
        }

        inorderTraversal(root.left, errorNodes);
        if (null != preNode && root.val < preNode.val) {
            if (errorNodes.isEmpty()) {
                errorNodes.add(preNode);
                errorNodes.add(root);
            } else {
                errorNodes.add(root);
            }
        }
        preNode = root;
        inorderTraversal(root.right, errorNodes);
    }

    @Override
    protected void beforeTest() {
        this.preNode = null;
    }
}
