package com.flaming.leetcode;

import com.flaming.model.TreeNode;

import java.util.ArrayList;
import java.util.List;

public class Question99_3 extends Question99{

    private List<TreeNode> errorNodes = new ArrayList<>();
    private TreeNode lastInOrderNode = null;

    @Override
    protected void recoverTree(TreeNode root) {
        if (null == root) {
            return;
        }

        TreeNode curNode = root;
        TreeNode preNode;
        while (null != curNode) {
            if (errorNodes.size() > 2) {
                break;
            }
            if (null == curNode.left) {
                visit(curNode);
                curNode = curNode.right;
                continue;
            }
            preNode = curNode.left;
            while (null != preNode.right && curNode != preNode.right) {
                preNode = preNode.right;
            }
            if (null == preNode.right) {
                preNode.right = curNode;
                curNode = curNode.left;
            } else {
                preNode.right = null;
                visit(curNode);
                curNode = curNode.right;
            }
        }

        if (errorNodes.size() == 3) {
            swap(errorNodes.get(0), errorNodes.get(2));
        } else {
            swap(errorNodes.get(0), errorNodes.get(1));
        }
    }

    private void visit(TreeNode root) {
        if (null == lastInOrderNode || root.val > lastInOrderNode.val) {
            lastInOrderNode = root;
            return;
        }

        if (errorNodes.isEmpty()) {
            errorNodes.add(lastInOrderNode);
        }
        errorNodes.add(root);
        lastInOrderNode = root;
    }

    private void swap(TreeNode node1, TreeNode node2) {
        int tmp = node1.val;
        node1.val = node2.val;
        node2.val = tmp;
    }

    @Override
    protected void beforeTest() {
        this.errorNodes = new ArrayList<>();
        this.lastInOrderNode = null;
    }
}
