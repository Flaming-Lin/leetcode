package com.flaming.model;

import java.util.Arrays;

public class ListNode {

    public int val;
    public ListNode next;
    public ListNode() {}
    public ListNode(int val) { this.val = val; }
    public ListNode(int val, ListNode next) { this.val = val; this.next = next; }
    public ListNode(int[] array) {
        if (array.length <= 0) {
            return;
        }
        if (array.length == 1) {
            this.val = array[0];
            this.next = null;
            return;
        }

        int[] nextArray = Arrays.copyOfRange(array, 1, array.length);
        this.val = array[0];
        this.next = new ListNode(nextArray);
    }

    @Override
    public String toString() {
        ListNode curNode = this;
        StringBuilder sb = new StringBuilder();
        while (null != curNode) {
            sb.append(curNode.val);
            sb.append(" ");
            curNode = curNode.next;
        }
        return sb.toString();
    }
}
