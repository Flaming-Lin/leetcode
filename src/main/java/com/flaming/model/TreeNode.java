package com.flaming.model;

import java.util.LinkedList;
import java.util.Queue;

public class TreeNode {

    public int val;
    public TreeNode left;
    public TreeNode right;

    public TreeNode(int x) {
        val = x;
    }

    public TreeNode(int val, TreeNode left, TreeNode right) {
        this.val = val;
        this.left = left;
        this.right = right;
    }

    @Override
    public String toString() {
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(this);
        StringBuilder sb = new StringBuilder();
        while (!queue.isEmpty()) {
            TreeNode curNode = queue.poll();
            sb.append(null == curNode ? "null" : curNode.val);
            sb.append(" ");
            if (null == curNode) {
                continue;
            }
            queue.offer(curNode.left);
            queue.offer(curNode.right);
        }
        return sb.toString();
    }

    public static TreeNode generateCompleteBinaryTree(int n) {
        if (n == 1) {
            return new TreeNode(1);
        }
        TreeNode root = new TreeNode(1);
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);
        for (int i = 2; i <= n; i+=2) {
            TreeNode curNode = queue.poll();
            curNode.left = new TreeNode(i);
            queue.offer(curNode.left);
            if (i + 1 > n) {
                break;
            }
            curNode.right = new TreeNode(i + 1);
            queue.offer(curNode.right);
        }
        return root;
    }

}
