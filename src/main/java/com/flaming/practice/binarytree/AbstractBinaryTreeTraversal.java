package com.flaming.practice.binarytree;

import com.flaming.model.TreeNode;

import java.util.List;

public abstract class AbstractBinaryTreeTraversal {

    protected abstract List<Integer> traversal(TreeNode root);

    public static void main(String[] args) {
        TreeNode root = new TreeNode(1,
                new TreeNode(2, new TreeNode(4), new TreeNode(5)),
                new TreeNode(3, new TreeNode(6), new TreeNode(7)));
        System.out.println("-------------- Result --------------");
        System.out.println("LevelOrder: \t\t\t" + invoke(root, LevelOrderTraversal.class));
        System.out.println("PreOrderRecursion: \t\t" + invoke(root, PreOrderRecursionTraversal.class));
        System.out.println("PreOrderNoRecursion: \t" + invoke(root, PreOrderNoRecursionTraversal.class));
        System.out.println("InOrderRecursion: \t\t" + invoke(root, InOrderRecursionTraversal.class));
        System.out.println("InOrderNoRecursion: \t" + invoke(root, InOrderNoRecursionTraversal.class));
        System.out.println("InOrderMorris: \t\t\t" + invoke(root, InOrderMorrisTraversal.class));
        System.out.println("PostOrderRecursion: \t" + invoke(root, PostOrderRecursionTraversal.class));
        System.out.println("PostOrderNoRecursion: \t" + invoke(root, PostOrderNoRecursionTraversal.class));
        System.out.println("PracticeTraversal: \t" + invoke(root, PracticeTraversal.class));
        System.out.print("-------------- Result --------------");
    }

    private static String invoke(TreeNode root, Class<? extends AbstractBinaryTreeTraversal> clazz) {
        try {
            return (clazz.newInstance()).traversal(root).toString();
        } catch (Exception e) {
            return "Error " + e.getMessage();
        }
    }

}
