package com.flaming.practice.binarytree;

import com.flaming.model.TreeNode;

import java.util.ArrayList;
import java.util.List;

public class InOrderMorrisTraversal extends AbstractBinaryTreeTraversal {

    /**
     * 在二叉树的中序遍历中, 无论递归还是非递归方式, 都需要 O(H) 的空间复杂度来记录待访问的节点。
     * 而 Morris 中序遍历充分利用了叶子节点的 null 右节点用来记录下一个待访问的节点, 以此实现了 O(1) 的空间复杂度。
     * 
     * 初始化 cur = root, pre = null
     * 1、若 cur.left 为空, 访问 cur, cur = cur.right （理解：没有左节点, 直接走下一步就是了）
     * 2、pre = cur.left, 寻找到 pre 的最右节点 x
     * 2.1、若 x.right != cur, 将 x.right = cur, cur = cur.left 
     *     （理解：x 一定是 cur 的左节点中序遍历的最后一个, 利用该 null 右节点指向 cur 来记录待访问节点）
     * 2.2、若 x.right = cur, x.right = null, 访问 cur, cur = cur.right
     */
    @Override
    protected List<Integer> traversal(TreeNode root) {
        if (null == root) {
            return new ArrayList<>();
        }

        List<Integer> results = new ArrayList<>();
        TreeNode curNode = root;
        TreeNode preNode;
        while (null != curNode) {
            preNode = curNode.left;
            if (null == preNode) {
                results.add(curNode.val);
                curNode = curNode.right;
                continue;
            }
            while (null != preNode.right && curNode != preNode.right) {
                preNode = preNode.right;
            }
            if (null == preNode.right) {
                preNode.right = curNode;
                curNode = curNode.left;
            } else {
                preNode.right = null;
                results.add(curNode.val);
                curNode = curNode.right;
            }
        }
        return results;
    }
}
