package com.flaming.practice.binarytree;

import com.flaming.model.TreeNode;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class InOrderNoRecursionTraversal extends AbstractBinaryTreeTraversal {

    @Override
    protected List<Integer> traversal(TreeNode root) {
        if (null == root) {
            return new ArrayList<>();
        }

        List<Integer> results = new ArrayList<>();
        Stack<TreeNode> stack = new Stack<>();
        while (null != root || !stack.isEmpty()) {
            while (null != root) {
                stack.push(root);
                root = root.left;
            }
            root = stack.pop();
            results.add(root.val);
            root = root.right;
        }
        return results;
    }
}
