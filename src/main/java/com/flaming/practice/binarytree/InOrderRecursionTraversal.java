package com.flaming.practice.binarytree;

import com.flaming.model.TreeNode;

import java.util.ArrayList;
import java.util.List;

public class InOrderRecursionTraversal extends AbstractBinaryTreeTraversal {

    @Override
    protected List<Integer> traversal(TreeNode root) {
        List<Integer> results = new ArrayList<>();
        traversal(root, results);
        return results;
    }

    private void traversal(TreeNode root, List<Integer> results) {
        if (null == root) {
            return;
        }
        traversal(root.left, results);
        results.add(root.val);
        traversal(root.right, results);
    }
}
