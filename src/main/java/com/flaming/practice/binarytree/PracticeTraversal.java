package com.flaming.practice.binarytree;

import com.flaming.model.TreeNode;

import java.util.*;

public class PracticeTraversal extends AbstractBinaryTreeTraversal {

    @Override
    protected List<Integer> traversal(TreeNode root) {
        /** Write your code here to practice */
        // DFS - inOrder - Morris
        if (null == root) {
            return new ArrayList<>();
        }

        List<Integer> results = new ArrayList<>();
        TreeNode curNode = root;
        TreeNode preNode = null;
        while (null != curNode) {
            preNode = curNode.left;
            if (null == preNode) {
                results.add(curNode.val);
                curNode = curNode.right;
                continue;
            }
            while (null != preNode.right && curNode != preNode.right) {
                preNode = preNode.right;
            }
            if (curNode != preNode.right) {
                preNode.right = curNode;
                curNode = curNode.left;
                continue;
            }
            results.add(curNode.val);
            curNode = curNode.right;
            preNode.right = null;
        }

        return results;
    }

}
