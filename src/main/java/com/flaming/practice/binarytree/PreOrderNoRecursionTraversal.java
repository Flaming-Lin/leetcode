package com.flaming.practice.binarytree;

import com.flaming.model.TreeNode;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class PreOrderNoRecursionTraversal extends AbstractBinaryTreeTraversal {

    @Override
    protected List<Integer> traversal(TreeNode root) {
        if (null == root) {
            return new ArrayList<>();
        }

        List<Integer> results = new ArrayList<>();
        Stack<TreeNode> stack = new Stack<>();
        stack.push(root);
        while (!stack.isEmpty()) {
            root = stack.pop();
            while (null != root) {
                results.add(root.val);
                stack.push(root.right);
                root = root.left;
            }
        }
        return results;
    }
}
